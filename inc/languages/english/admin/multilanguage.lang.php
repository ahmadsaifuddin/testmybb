<?php

$l['multilanguage'] = "Multilanguage Forums";
$l['multilanguage_desc'] = "It allows you to change names and descriptions of your forums depending on the language.";
$l['multilanguage_saved'] = "Forums translated successfully!";
$l['multilanguage_error'] = "An error occured while saving!";
$l['multilanguage_translation'] = "Translation in {1}";
$l['multilanguage_default'] = "Default";
$l['multilanguage_title'] = "Title";
$l['multilanguage_description'] = "Description";
$l['multilanguage_save'] = "Save";
$l['multilanguage_translate'] = "Translate";
$l['multilanguage_packets'] = "Language Packs";