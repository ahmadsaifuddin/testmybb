<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: index.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['boardstats'] = "掲示板の統計";
$l['new_posts'] = "新着記事のあるフォーラム";
$l['no_new_posts'] = "新着記事のないフォーラム";
$l['forum_locked'] = "締め切られているフォーラム";
$l['forum_unapproved_posts_count'] = "拒否された記事が{1}個あります。";
$l['forum_unapproved_post_count'] = "拒否された記事が1個あります。";
$l['forum_unapproved_threads_count'] = "拒否されたスレッドが{1}個あります。";
$l['forum_unapproved_thread_count'] = "拒否されたスレッドが1個あります。";
$l['markread'] = "すべてのフォーラムを既読にする";
$l['forumteam'] = "フォーラムチーム";
$l['forumstats'] = "フォーラムの統計";
$l['todays_birthdays'] = "今日が誕生日";
$l['birthdayhidden'] = "非表示";
$l['quick_login'] = "クイックログイン:";
$l['index_logout'] = "ログアウト";
$l['private_messages'] = "プライベートメッセージ";
$l['pms_new'] = "前回のログアウト後、{1}通の新しいメッセージが届いています。";
$l['pms_unread_total'] = "(フォルダ全体で{1}通の未読メッセージと合計{2}通のメッセージがあります。)";
$l['stats_posts_threads'] = "メンバー全体で{2}個のスレッドに合計{1}個の記事を投稿しました。";
$l['stats_numusers'] = "登録されているメンバーは{1}名です。";
$l['stats_newestuser'] = "最新の登録メンバー: <b>{1}</b>";
$l['stats_mostonline'] = "同時オンラインの最大数は、{2}{3}の{1}名";
$l['whos_online'] = "オンラインユーザ";
$l['complete_list'] = "全員を表示";
$l['online_online_plural'] = "ユーザ";
$l['online_online_singular'] = "ユーザ";
$l['online_member_plural'] = "登録メンバ";
$l['online_member_singular'] = "登録メンバ";
$l['online_anon_plural'] = "は";
$l['online_anon_singular'] = "は";
$l['online_guest_plural'] = "ゲスト";
$l['online_guest_singular'] = "ゲスト";
$l['online_note'] = "過去{3}分間に{1}{2}がアクティブ ({5}{4}人中、非表示が{6}人、{9}が{8}人)";
$l['subforums'] = "<strong>サブフォーラム:</strong>";
?>
