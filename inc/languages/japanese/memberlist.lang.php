<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: memberlist.lang.php 5828 2012-05-08 16:06:16Z Tomm $
 */

$l['nav_memberlist'] = "メンバーリスト";
$l['memberlist_disabled'] = "管理者が無効に設定しているため、メンバーリストはご利用になれません。";
$l['member_list'] = "メンバーリスト";
$l['avatar'] = "アバター";
$l['username'] = "ユーザ名";
$l['joined'] = "登録日";
$l['lastvisit'] = "最後のログイン";
$l['posts'] = "投稿数";
$l['referrals'] = "紹介者";
$l['search_members'] = "メンバーリストを検索";
$l['website'] = "Webサイト";
$l['sort_by'] = "並べ替え";
$l['contains'] = "次の語を含む:";
$l['sort_by_username'] = "ユーザ名で並べ替え";
$l['sort_by_regdate'] = "登録日で並べ替え";
$l['sort_by_lastvisit'] = "最後のログインで並べ替え";
$l['sort_by_posts'] = "投稿数で並べ替え";
$l['sort_by_referrals'] = "紹介者で並べ替え";
$l['order_asc'] = "昇順";
$l['order_desc'] = "降順";
$l['forumteam'] = "フォーラムチームを表示";
$l['advanced_search'] = "高度な検索";
$l['search_member_list'] = "メンバーリストを検索";
$l['search_criteria'] = "検索条件";
$l['begins_with'] = "次の語で始まる";
$l['username_contains'] = "次の語を含む";
$l['search_website'] = "WebサイトURL";
$l['search_aim'] = "AIMスクリーンネーム";
$l['search_msn'] = "MSN ID";
$l['search_yahoo'] = "Yahoo! ID";
$l['search_icq'] = "ICQナンバー";
$l['search_options'] = "検索オプション";
$l['per_page'] = "1ページに表示する数";
$l['search'] = "検索";
$l['error_no_members'] = "<p>検索条件に一致するメンバーが見つかりませんでした。</p><p>検索条件を変えて、お試しください。</p>";
$l['a'] = "A";
$l['b'] = "B";
$l['c'] = "C";
$l['d'] = "D";
$l['e'] = "E";
$l['f'] = "F";
$l['g'] = "G";
$l['h'] = "H";
$l['i'] = "I";
$l['j'] = "J";
$l['k'] = "K";
$l['l'] = "L";
$l['m'] = "M";
$l['n'] = "N";
$l['o'] = "O";
$l['p'] = "P";
$l['q'] = "Q";
$l['r'] = "R";
$l['s'] = "S";
$l['t'] = "T";
$l['u'] = "U";
$l['v'] = "V";
$l['w'] = "W";
$l['x'] = "X";
$l['y'] = "Y";
$l['z'] = "Z";
?>
