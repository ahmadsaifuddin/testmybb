<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 *
 * $Id: reputation.lang.php 5750 2012-03-09 12:21:10Z Tomm $
 */

$l['nav_reputation'] = "評判";
$l['nav_profile'] = "{1}のプロフィール";
$l['reputation_disabled'] = "管理者が無効にしているため、評判システムはご利用になれません。";
$l['reputation'] = "評判";
$l['error'] = "エラー";
$l['add_no_uid'] = "評価するユーザが指定されていません。";
$l['add_no_permission'] = "ユーザを評価する権限がありません。";
$l['add_disabled'] = "このユーザグループに所属するユーザは評価できません。";
$l['add_yours'] = "自分自身を評価することはできません。";
$l['add_invalidpower'] = "選択された評価が間違っています。";
$l['add_maxperday'] = "1日に評価できる回数の上限に達したため、今日はこれ以上評価できません。";
$l['add_maxperuser'] = "1日に評価できる回数の上限に達したため、今日はこれ以上このユーザを評価することはできません。";
$l['add_maxperthread'] = "1日に（同じスレッド内で）評価できる回数の上限に達したため、今日はこれ以上このユーザを評価することはできません。";
$l['add_no_comment'] = "ユーザを評価するには最低10文字以上のコメント入力が必要です。";
$l['add_toolong'] = "{1}文字以内で理由を記入してください。";
$l['add_negative_disabled'] = "管理者によりマイナス評価ができないよう設定されています。";
$l['add_neutral_disabled'] = "管理者によりどちらでもない評価はできないよう設定されています。";
$l['add_positive_disabled'] = "管理者によりプラス評価ができないよう設定されています。";
$l['add_all_rep_disabled'] = "管理者により評価システムが無効に設定されています。ユーザを評価することはできません。";
$l['reputation_report'] = "{1}の評判レポート";
$l['reputation_members'] = "メンバーからの評判:";
$l['reputation_posts'] = "記事の評判:";
$l['summary'] = "概要";
$l['total_reputation'] = "総合的な評判";
$l['post_reputation'] = "記事の評判";
$l['positive_count'] = "プラス";
$l['neutral_count'] = "どちらでもない";
$l['negative_count'] = "マイナス";
$l['last_week'] = "先週";
$l['last_month'] = "先月";
$l['last_6months'] = "過去6か月";
$l['comments'] = "コメント";
$l['close_window'] = "ウィンドウを閉じる";
$l['go_back'] = "戻る";
$l['add_reputation_vote'] = "{1}を評価する";
$l['add_reputation_to_post'] = "この評価は{1}の記事に対して行われました。<br />";
$l['neg_rep_disabled'] = "<span class=\"smalltext\">* - <em>マイナス評価は無効に設定されています</em></span>";
$l['pos_rep_disabled'] = "<span class=\"smalltext\">* - <em>プラス評価は無効に設定されています</em></span>";
$l['neu_rep_disabled'] = "<span class=\"smalltext\">* - <em>どちらでもない評価は無効に設定されています</em></span>";
$l['no_comment_needed'] = "ユーザのプロフィールからリンクされている記事を評価しようとしています。コメントは必須ではありませんが、下の欄に入力することも可能です。<br />";
$l['no_comment'] = "[コメントなし]";
$l['vote_added'] = "追加された評価";
$l['vote_updated'] = "更新された評価";
$l['vote_deleted'] = "削除された評価";
$l['vote_added_message'] = "ユーザへの評価を追加しました。";
$l['vote_updated_message'] = "ユーザへの評価を更新しました。";
$l['vote_deleted_message'] = "このユーザへの評価を削除しました。";
$l['update_reputation_vote'] = "{1}への評価を更新しました。";
$l['positive'] = "プラス";
$l['negative'] = "マイナス";
$l['neutral'] = "どちらでもない";
$l['user_comments'] = "{1}へのコメント:";
$l['add_vote'] = "評価を追加";
$l['update_vote'] = "評価を更新";
$l['delete_vote'] = "評価を削除";
$l['power_positive'] = "プラス ({1})";
$l['power_neutral'] = "どちらでもない";
$l['power_negative'] = "マイナス ({1})";
$l['show_all'] = "すべての評価を見る";
$l['show_positive'] = "プラス評価を見る";
$l['show_neutral'] = "どちらでもない評価を見る";
$l['show_negative'] = "マイナス評価を見る";
$l['sort_updated'] = "最終更新で並べ替え";
$l['sort_username'] = "ユーザ名で並べ替え";
$l['last_updated'] = "最終更新 {1} {2}";
$l['postrep_post'] = "投稿数";
$l['postrep_given'] = "{1}の評価<br />";
$l['no_reputation_votes'] = "指定された条件に該当する評判を持つユーザが存在しません。";
$l['delete_reputation_confirm'] = "本当にこの評価を削除しますか？";
$l['delete_reputation_log'] = "{1} (UID: {2}) の評価を削除";
$l['reputations_disabled_group'] = "このグループではユーザの評価機能が使えません。";
$l['rate_user'] = "ユーザを評価";
?>