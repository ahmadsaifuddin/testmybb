<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: archive.lang.php 5365 2011-02-16 21:59:02Z MattR $
 */

$l['archive_fullversion'] = "フル表示版:";
$l['archive_replies'] = "返信";
$l['archive_reply'] = "返信";
$l['archive_pages'] = "ページ:";
$l['archive_note'] = "このページは簡易表示版です。適切なフォーマットで閲覧するには、<a href=\"{1}\">フル表示版</a>をご覧ください。";
$l['archive_reference_urls'] = "参照URL";
$l['archive_nopermission'] = "恐れ入りますが、権限がないためこのリソースにアクセスできません。";
$l['error_nothreads'] = "このフォーラムには、まだスレッドがありません。";
$l['error_unapproved_thread'] = "このスレッドは拒否されています。スレッドの内容を表示するには<a href=\"{1}\">フル表示版</a>をご覧ください。";
$l['archive_not_found'] = "指定されたページは存在しません。";
?>