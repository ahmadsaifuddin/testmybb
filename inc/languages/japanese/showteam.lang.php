<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: showteam.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['nav_showteam'] = "フォーラムチーム";
$l['forum_team'] = "フォーラムチーム";
$l['moderators'] = "モデレータ";
$l['mod_username'] = "ユーザ名";
$l['mod_forums'] = "フォーラム";
$l['mod_email'] = "メール";
$l['mod_pm'] = "PM";
$l['uname'] = "ユーザ名";
$l['email'] = "メール";
$l['pm'] = "PM";
$l['group_leaders'] = "リーダー";
$l['group_members'] = "メンバー";
$l['no_members'] = "このグループにはメンバーがいません。";
$l['error_noteamstoshow'] = "表示できるフォーラムスタッフがいません。";
?>