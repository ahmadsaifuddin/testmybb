<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: xmlhttp.lang.php 5701 2011-12-07 08:48:12Z Tomm $
 */

$l['no_new_subject'] = "件名が入力されていません。";
$l['post_moderation'] = "モデレートされるまで記事は保留されます。";
$l['post_doesnt_exist'] = "指定された記事は存在しません。";
$l['thread_doesnt_exist'] = "指定されたスレッドは存在しません。";
$l['thread_closed_edit_subjects'] = "このスレッドは締め切られているため、件名を編集できません。";
$l['no_permission_edit_subject'] = "このスレッドの件名を編集する権限がありません。";
$l['thread_closed_edit_message'] = "このスレッドは締め切られているため、その中のメッセージは編集できません。";
$l['no_permission_edit_post'] = "このメッセージを編集する権限がありません。";
$l['edit_time_limit'] = "記事の編集は投稿後{1}分間以内に制限されています。";
$l['postbit_edited'] = "この記事が最後に編集されたのは{1} {2}です。編集者は";
$l['save_changes'] = "変更を保存";
$l['cancel_edit'] = "編集をキャンセル";
$l['captcha_not_exists'] = "再表示しようとしている画像認証の画像が存在しません。";
$l['captcha_valid_not_exists'] = "確認しようとしている画像認証の画像が存在しません。";
$l['captcha_does_not_match'] = "入力された画像認証コードが間違っています。画像に表示されたとおり正確に入力してください。";
$l['captcha_matches'] = "入力された画像認証コードで認証されました。";
$l['banned_username'] = "入力されたユーザ名は管理者により使用禁止されています。";
$l['banned_characters_username'] = "ユーザ名に使用できない文字が含まれています。";
$l['complex_password_fails'] = "パスワードには大文字と小文字と数字をすべて含めてください。";
$l['username_taken'] = "{1}は別のメンバーによりすでに登録済みです。";
$l['username_available'] = "{1}は利用可能です。";
$l['invalid_username'] = "{1}は登録されたメンバーではありません。";
$l['valid_username'] = "{1}は正しい紹介者です。";
$l['buddylist_error'] = "友人リストに登録がありません。この機能を使う前に友人を登録してください。";
$l['close'] = "閉じる";
$l['select_buddies'] = "友人を選択";
$l['select_buddies_desc'] = "友人リストから宛先に追加するには、下から選択して「OK」をクリックしてください。複数指定可能です。";
$l['selected_recipients'] = "選択した宛先";
$l['ok'] = "OK";
$l['cancel'] = "キャンセル";
$l['online'] = "オンライン";
$l['offline'] = "オフライン";
$l['edited_post'] = "編集済みの投稿";
$l['usergroup'] = "ユーザグループ";
?>