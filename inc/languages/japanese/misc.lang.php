<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: misc.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['nav_helpdocs'] = "ヘルプ";
$l['nav_smilies'] = "スマイリーリスト";
$l['nav_syndication'] = "最新のスレッド配信 (RSS)";
$l['aol_im'] = "AOL IM";
$l['msn'] = "MSNメッセンジャー";
$l['yahoo_im'] = "Yahoo IM";
$l['msn_messenger_center'] = "MSNメッセンジャーセンター";
$l['msn_address_is'] = "{1}のMSN IDは";
$l['yahoo_center'] = "Yahoo!センター";
$l['send_y_message'] = "{1}にYahoo!メッセージを送信";
$l['view_y_profile'] = "{1}のYahoo!プロフィールを表示";
$l['aim_center'] = "AOL IMセンター";
$l['download_aim'] = "AIMをダウンロード";
$l['aim_remote'] = "AIMリモート";
$l['send_me_instant'] = "インスタントメッセージを受付中";
$l['add_me_buddy_list'] = "友人リストに追加してください";
$l['add_remote_to_page'] = "リモートをページに追加";
$l['download_aol_im'] = "AOL IMをダウンロード";
$l['buddy_list'] = "友人リスト";
$l['online'] = "オンライン";
$l['offline'] = "オフライン";
$l['delete_buddy'] = "X";
$l['pm_buddy'] = "プライベートメッセージを送信";
$l['last_active'] = "<strong>最終ログイン:</strong> {1}";
$l['close'] = "閉じる";
$l['no_buddies'] = "<em>現在、友人リストに登録がありません。友人の登録は、ユーザCPまたはユーザのプロファイルから行えます。</em>";
$l['help_docs'] = "ヘルプ";
$l['smilies_listing'] = "スマイリーリスト";
$l['name'] = "名前";
$l['abbreviation'] = "省略形";
$l['click_to_add'] = "メッセージに挿入するスマイリーをクリックしてください。";
$l['close_window'] = "ウィンドウを閉じる";
$l['who_posted'] = "投稿者";
$l['total_posts'] = "合計:";
$l['user'] = "ユーザ";
$l['num_posts'] = "投稿数";
$l['forum_rules'] = "{1} - ルール";
$l['error_invalid_limit'] = "入力されたフィード項目の制限が間違っています。正しい制限を入力してください。";
$l['syndication'] = "最新のスレッド";
$l['syndication_generated_url'] = "生成されたフィードURL:";
$l['syndication_note'] = "ここでは特定のRSS配信へのリンクを作成できます。全フォーラム、またはフォーラムごと、または指定した数のフォーラムのリンクが作成できます。このリンクを<a href=\"http://www.sharpreader.net/\">SharpReader</a>のようなRSSリーダに読み込ませて利用
できます。<i><a href=\"http://www.xml.com/pub/a/2002/12/18/dive-into-xml.html\">What is RSS?</a></i>";
$l['syndication_forum'] = "フィードするフォーラム:";
$l['syndication_forum_desc'] = "右からフォーラムを選択してください。CTRLキーを使えば複数選択できます。";
$l['syndication_version'] = "フィードバージョン:";
$l['syndication_version_desc'] = "生成するフィードのバージョンを選択してください。";
$l['syndication_version_atom1'] = "Atom 1.0";
$l['syndication_version_rss2'] = "RSS 2.00 (デフォルト)";
$l['syndication_generate'] = "フィードURLを生成";
$l['syndication_limit'] = "制限:";
$l['syndication_limit_desc'] = "一度にダウンロードするスレッド数。50以下で指定してください。";
$l['syndication_threads_time'] = "一度にフィードするスレッド数";
$l['syndicate_all_forums'] = "すべてのフォーラムをフィード";
$l['redirect_markforumread'] = "選択したフォーラムを既読にしました。";
$l['redirect_markforumsread'] = "すべてのフォーラムを既読にしました。";
$l['redirect_forumpasscleared'] = "保存していたこのフォーラムのパスワードを消去しました。";
$l['redirect_cookiescleared'] = "クッキーをすべて消去しました。";
$l['error_invalidimtype'] = "このユーザのプロフィールには、指定されたインスタントメッセンジャーのアカウントがありません。";
$l['error_invalidhelpdoc'] = "指定されたヘルプが存在しません。";
$l['error_invalidkey'] = "クッキーを消去するための認証に失敗しました。悪意あるJavaScriptが自動的に消去しようとした可能性があります。本当にクッキーを消去したい場合には、ヘルプの「MyBBでのクッキーの使用」をご覧ください。";
$l['dst_settings_updated'] = "夏時間は自動的に調整されます。<br /><br />フォーラム一覧に戻ります。";
?>
