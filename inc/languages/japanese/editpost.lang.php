<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: editpost.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['nav_editpost'] = "記事を編集";
$l['edit_post'] = "記事を編集";
$l['delete_post'] = "記事を削除";
$l['delete_q'] = "削除しますか？";
$l['delete_1'] = "この記事を削除するには、左のチェックボックスにチェックしてから右のボタンをクリックしてください。";
$l['delete_2'] = "<b>注意:</b> この記事がスレッドへの最初の投稿の場合、これを削除するとスレッド全体が削除されます。";
$l['subject'] = "件名";
$l['your_message'] = "本文";
$l['post_options'] = "記事オプション:";
$l['options_sig'] = "<strong>署名:</strong> 署名を挿入（登録ユーザのみ）";
$l['options_emailnotify'] = "<strong>メールによる通知:</strong> 新しい返信がついたときにメールで通知する (登録ユーザのみ)";
$l['options_disablesmilies'] = "<strong>スマイリーを無効:</strong> この記事ではスマイリーを無効にする";
$l['preview_post'] = "プレビュー";
$l['update_post'] = "更新";
$l['poll'] = "アンケート:";
$l['poll_desc'] = "オプションで、スレッドにアンケートを追加できます。";
$l['poll_check'] = "アンケートを使用する";
$l['num_options'] = "選択肢の数:";
$l['max_options'] = "(最大: {1})";
$l['delete_now'] = "今すぐ削除";
$l['edit_time_limit'] = "記事の編集ができません。管理者により、記事の編集は投稿後{1}分以内に制限されています。";
$l['no_prefix'] = "プリフィクスがありません。";
$l['redirect_nodelete'] = "「削除」にチェックされていなかったため、記事は削除されませんでした。";
$l['redirect_postedited'] = "記事の編集が完了しました。";
$l['redirect_postedited_redirect'] = "スレッドに戻ります。";
$l['redirect_postedited_poll'] = "記事の編集が完了しました。<br />アンケートを使用する指定がされているため、アンケート作成ページに移動します。";
$l['error_invalidpost'] = "指定されたアドレスが間違っています。記事が存在することを確認の上、もう一度お試しください。";
$l['redirect_threaddeleted'] = "スレッドが削除されました。<br />すぐにフォーラムに移動します。";
$l['redirect_postdeleted'] = "記事は削除されました。<br />すぐにスレッドに移動します。";
$l['redirect_threadclosed'] = "モデレータがフォーラムを締め切ったため、このスレッド内の記事は編集できません。";
$l['redirect_post_moderation'] = "管理者の指定により、記事の編集にはモデレータの承認が必要です。すぐにスレッドに戻ります。";
$l['redirect_thread_moderation'] = "管理者の指定により、スレッドの編集にはモデレータの承認が必要です。すぐにフォーラム一覧に戻ります。";
$l['thread_deleted'] = "削除されたスレッド";
$l['post_deleted'] = "削除された記事";
?>
