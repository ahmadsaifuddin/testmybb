<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: managegroup.lang.php 5702 2011-12-07 09:01:51Z Tomm $
 */

$l['nav_group_management'] = "{1} グループ管理";
$l['nav_join_requests'] = "参加リクエスト";
$l['nav_group_memberships'] = "グループ";
$l['not_leader_of_this_group'] = "グループリーダではありません。";
$l['invalid_group'] = "このユーザグループは存在しません。";
$l['pending_requests'] = "保留中の参加リクエスト";
$l['num_requests_pending'] = "現在、このグループへの {1} 件の参加リクエストが保留中です。";
$l['group_management'] = "グループ管理";
$l['members_of'] = "「{1}」のメンバー";
$l['user_name'] = "ユーザ名";
$l['contact'] = "連絡先";
$l['reg_date'] = "登録済";
$l['post_count'] = "投稿数";
$l['remove_selected'] = "選択したユーザをグループから削除";
$l['add_member'] = "「{1}」にメンバーを追加";
$l['add_member_submit'] = "メンバーをグループに追加";
$l['join_requests'] = "参加リクエスト";
$l['join_requests_title'] = "「{1}」の参加リクエスト";
$l['leader'] = "(リーダ)";
$l['reason'] = "理由";
$l['accept'] = "許可";
$l['ignore'] = "無視";
$l['decline'] = "拒否";
$l['action_requests'] = "アクションを実行";
$l['join_requests_moderated'] = "参加リクエストがモデレートされました。<br />すぐにリクエストリストに戻ります。";
$l['no_requests'] = "保留中の参加リクエストはありません。";
$l['no_users'] = "このグループにはユーザがいません。";
$l['user_added'] = "ユーザグループにユーザが追加されました。";
$l['users_removed'] = "選択したユーザがユーザグループから削除されました。";
$l['group_no_members'] = "現在、このグループにはメンバーがいません。<br />グループ管理に戻るには<a href=\"usercp.php?action=usergroups\">ここ</a>をクリックしてください。";
$l['group_public_moderated'] = "このユーザグループは公開されていて、誰でも参加できます。参加リクエストはすべてグループリーダがモデレートします。";
$l['group_public_not_moderated'] = "このユーザグループは公開されていて、誰でも参加できます。";
$l['group_private'] = "このユーザグループはプライベートなグループです。リーダが追加したユーザのみ参加することができます。";
$l['group_default'] = "このユーザグループはコアユーザのグループです。";
$l['group_leaders'] = "グループリーダ";
$l['no_users_selected'] = "削除するユーザが選択されていません。<br />戻って、このグループから削除したいユーザを選択してください。";
$l['error_alreadyingroup'] = "指定されたユーザは、このグループに参加済みです。";
?>