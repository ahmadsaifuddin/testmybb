<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 *
 * $Id$
 */

$l['userdata_missing_username'] = "ユーザ名が入力されていません。ユーザ名を入力してください。";
$l['userdata_banned_username'] = "指定されたユーザ名では登録できません。別のユーザ名を入力してください。";
$l['userdata_bad_characters_username'] = "指定されたユーザ名には、使用できない文字が含まれています。別のユーザ名を入力してください。";
$l['userdata_invalid_username_length'] = "指定されたユーザ名は長さが適切ではありません。{1}文字以上{2}文字以下で入力してください。";
$l['userdata_invalid_usertitle_length'] = "カスタムユーザタイトルの長さが適切ではありません。{1}文字以上{2}文字以下で入力してください。";
$l['userdata_username_exists'] = "指定されたユーザ名は既に使われています。別のユーザ名を入力してください。";
$l['userdata_invalid_password_length'] = "指定されたパスワードは長さが適切ではありません。{1}文字以上{2}文字以下で入力してください。";
$l['userdata_no_complex_characters'] = "指定されたパスワードは十分な強度がありません。最低 {1} 文字以上かつ、大文字と小文字と数字を含むパスワードを設定してください。";
$l['userdata_passwords_dont_match'] = "確認用のパスワードが、最初に入力したパスワードと一致しません。正しいパスワードを入力してください。";
$l['userdata_missing_email'] = "メールアドレスが入力されていません。メールアドレスを入力してください。";
$l['userdata_invalid_email_format'] = "メールアドレスが間違っています。正しいメールアドレスを入力してください。";
$l['userdata_emails_dont_match'] = "確認用のメールアドレスが、最初に入力したメールアドレスと一致しません。正しいメールアドレスを入力してください。";
$l['userdata_banned_email'] = "指定されたメールアドレスは、現在使用が許可されていません。別のメールアドレスを入力してください。";
$l['userdata_email_already_in_use'] = "指定されたメールアドレスは、すでに登録済のメンバーが使用しています。別のメールアドレスを入力してください。";
$l['userdata_dob_required'] = "誕生日が入力されていません。誕生日を入力してください。誕生日と年齢は、他のユーザから見られないよう後から設定可能です。";
$l['userdata_invalid_website'] = "入力されたWebサイトのアドレスが間違っています。正しいアドレスを入力するか、空欄のままにしてください。";
$l['userdata_invalid_icq_number'] = "入力されたICQナンバーが間違っています。正しいICQナンバーを入力するか、空欄のままにしてください。";
$l['userdata_invalid_msn_address'] = "入力されたMSN IDが間違っています。正しいMSN IDを入力するか、空欄のままにしてください。";
$l['userdata_invalid_birthday'] = "入力された誕生日が間違っています。正しい誕生日を入力するか、空欄のままにしてください。";
$l['userdata_invalid_birthday_coppa'] = "年齢確認のため、生まれた年を入力してください。年齢と誕生日は、プロフィールのオプションで他の人から見られないよう設定することが可能です。";
$l['userdata_invalid_birthday_coppa2'] = "この掲示板に登録できるのは、13歳以上です。管理者に連絡してください。";
$l['userdata_invalid_birthday_privacy'] = "誕生日の公開オプションを正しく選択してください。";
$l['userdata_invalid_referrer'] = "入力された紹介メンバーは存在しません。紹介メンバー名を正しく入力するか、空欄のままにしてください。";
$l['userdata_invalid_language'] = "選択された言語は存在しません。言語を正しく選択してください。";
$l['userdata_missing_returndate'] = "帰宅日に指定する情報が不足しています。年月日それぞれの欄に入力してください。";
$l['userdata_missing_required_profile_field'] = "\"{1}\" のオプションが未入力です。この欄に入力してください。";
$l['userdata_bad_profile_field_values'] = "\"{1}\" に選択したオプションが間違っています。正しく選択してください。";
$l['userdata_max_limit_reached'] = "\"{1}\" に入力された文字数が無効です。{2}文字以下で入力してください。";
$l['userdata_invalid_checkfield'] = "このフォームはスパムボットによる送信と判断されました。間違いであれば、管理者に連絡してください。";
$l['userdata_invalid_postnum'] = "入力された投稿数が間違っています。正しい投稿数を入力するか、空欄のままにしてください。";
?>
