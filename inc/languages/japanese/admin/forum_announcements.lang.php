<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id$
 */

$l['forum_announcements'] = "フォーラム告知";
$l['forum_announcements_desc'] = "ここでは掲示板上の告知を管理できます。全体告知はすべてのフォーラムで表示され、フォーラムごとに設置された告知はそのフォーラムと子フォーラムに表示されます。";
$l['add_announcement'] = "告知を追加";
$l['add_announcement_desc'] = "ここではフォーラムごとの告知、または全体告知を追加できます。";
$l['update_announcement'] = "告知を保存";
$l['update_announcement_desc'] = "ここでは告知の内容を更新できます。";
$l['start_date_desc'] = "ここで指定する日時 (GMT) 以降に、選択したフォーラムに告知を表示します。";
$l['end_date_desc'] = "ここで指定する日時 (GMT) に告知がフォーラムに表示されなくなります。終了日を設定せずに表示し続けることも、終了日を設定して満了させることもできます。";
$l['forums_to_appear_in_desc'] = "ここで選択したフォーラムに告知が表示されます。告知は選択したフォーラムおよび、その下のすべてのフォーラムに表示されます。";
$l['announcement'] = "告知";
$l['global_announcements'] = "全体告知";
$l['no_global_announcements'] = "現在、フォーラム全体告知はありません。";
$l['no_forums'] = "現在、告知を設定したフォーラムはありません。";
$l['confirm_announcement_deletion'] = "本当にこの告知を削除しますか？";
$l['success_announcement_deleted'] = "選択された告知を削除しました。";
$l['success_added_announcement'] = "指定された告知を作成しました。";
$l['success_updated_announcement'] = "選択された告知を更新しました。";
$l['error_invalid_announcement'] = "正しい告知を入力してください。";
$l['error_missing_title'] = "タイトルが入力されていません。";
$l['error_missing_message'] = "本文が入力されていません。";
$l['error_missing_forum'] = "フォーラムが選択されていません。";
$l['error_invalid_start_date'] = "告知の開始日が間違っています。";
$l['error_invalid_end_date'] = "告知の終了日が間違っています。";
$l['error_end_before_start'] = "終了日には開始日より後の日付を指定してください。";
$l['add_an_announcement'] = "告知を追加";
$l['update_an_announcement'] = "告知を更新";
$l['save_announcement'] = "告知を保存";
$l['title'] = "タイトル";
$l['start_date'] = "開始日";
$l['end_date'] = "終了日";
$l['message'] = "本文";
$l['forums_to_appear_in'] = "告知を表示するフォーラム";
$l['allow_html'] = "HTMLを使用しますか？";
$l['allow_mycode'] = "MyCodeを表示しますか？";
$l['allow_smilies'] = "スマイリーを表示しますか？";
$l['time'] = "時刻:";
$l['set_time'] = "終了日時を設定";
?>