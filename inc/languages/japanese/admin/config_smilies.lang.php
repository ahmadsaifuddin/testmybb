<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_smilies.lang.php 5833 2012-05-24 08:39:00Z Tomm $
 */

$l['smilies'] = "スマイリー";
$l['manage_smilies'] = "スマイリーを管理";
$l['manage_smilies_desc'] = "ここではスマイリーの編集や削除ができます。";
$l['add_smilie'] = "新しいスマイリーを追加";
$l['add_smilie_desc'] = "ここでは新しいスマイリーをひとつずつ追加できます。";
$l['add_multiple_smilies'] = "スマイリーをまとめて追加";
$l['add_multiple_smilies_desc'] = "ここでは新しいスマイリーをまとめて追加できます。";
$l['edit_smilie'] = "スマイリーを編集";
$l['edit_smilie_desc'] = "ここではスマイリーをひとつずつ編集できます。";
$l['mass_edit'] = "まとめて編集";
$l['mass_edit_desc'] = "ここではすべてのスマイリーを一度にまとめて編集できます。";
$l['no_smilies'] = "現在、フォーラムには顔文字がありません。";
$l['image'] = "画像";
$l['name'] = "名前";
$l['text_replace'] = "置換テキスト";
$l['image_path'] = "画像へのパス";
$l['image_path_desc'] = "スマイリー画像へのパスです。";
$l['order'] = "順";
$l['display_order'] = "表示する順";
$l['display_order_desc'] = "一覧に表示するスマイリーの順。他のスマイリーとかぶらない数字を指定してください。.";
$l['mass_edit_show_clickable'] = "クリック選択表示";
$l['show_clickable'] = "クリック選択できる一覧上に表示しますか？";
$l['show_clickable_desc'] = "記事のエディタ上に表示するクリック可能なスマイリー一覧上に、このスマイリーを表示しますか？";
$l['include'] = "追加";
$l['path_to_images'] = "画像へのパス";
$l['path_to_images_desc'] = "画像が入っているフォルダへのパス。";
$l['smilie_delete'] = "削除";
$l['save_smilie'] = "スマイリーを保存";
$l['save_smilies'] = "スマイリーを保存";
$l['show_smilies'] = "スマイリーを表示";
$l['reset'] = "リセット";
$l['error_missing_name'] = "スマイリーの名前が入力されていません。";
$l['error_missing_text_replacement'] = "スマイリーの置換テキストが入力されていません。";
$l['error_missing_path'] = "スマイリーへのパスが入力されていません。";
$l['error_missing_path_multiple'] = "パスが入力されていません。";
$l['error_missing_order'] = "スマイリーの表示順が入力されていません。";
$l['error_missing_clickable'] = "クリック選択一覧に表示するかどうかが選択されていません。";
$l['error_no_smilies'] = "指定されたディレクトリ内にスマイリーが存在しません。またはそのディレクトリ内のスマイリーはすべて登録済みです。";
$l['error_no_images'] = "選択されたディレクトリには画像がありません。";
$l['error_none_included'] = "追加するスマイリーが選択されていません。";
$l['error_invalid_path'] = "入力されたパスが間違っています。";
$l['error_invalid_smilie'] = "選択されたスマイリーは存在しません。";
$l['success_smilie_added'] = "スマイリーを選択しました。";
$l['success_multiple_smilies_added'] = "選択されたスマイリーを追加しました。";
$l['success_smilie_updated'] = "スマイリーを更新しました。";
$l['success_multiple_smilies_updated'] = "スマイリーを更新しました。";
$l['success_smilie_deleted'] = "スマイリーを削除しました。";
$l['success_mass_edit_updated'] = "スマイリーを更新しました。";
$l['confirm_smilie_deletion'] = "本当にこのスマイリーを削除しますか？";
?>