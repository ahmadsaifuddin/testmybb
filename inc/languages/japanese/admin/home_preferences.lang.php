<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: home_preferences.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['preferences_and_personal_notes'] = "環境設定 &amp; 個人用メモ";
$l['prefs_and_personal_notes_description'] = "ここではAdmin Control Panel (管理者CP) の環境設定と自分自身の個人用メモを管理できます。";
$l['preferences'] = "環境設定";
$l['global_preferences'] = "全体設定";
$l['acp_theme'] = "管理者CPテーマ";
$l['select_acp_theme'] = "管理者CPで使用するテーマを選択してください。";
$l['notes_not_shared'] = "このメモは他の管理者とは共有されません。";
$l['save_notes_and_prefs'] = "個人用メモ & 環境設定を保存";
$l['personal_notes'] = "個人用メモ";
$l['codepress'] = "Codepressの On / Off";
$l['use_codepress_desc'] = "不具合が起きた場合や、表示を軽くするために、Codepress (テンプレートやスタイルシートの編集で構文をハイライト表示するために使われます) の使用をOn/Offできます。";
$l['success_preferences_updated'] = "環境設定を更新しました。";
?>