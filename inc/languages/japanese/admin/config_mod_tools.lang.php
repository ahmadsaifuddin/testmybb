<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_mod_tools.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */
 
$l['mod_tools'] = "モデレータツール";
$l['thread_tools'] = "スレッドツール";
$l['thread_tools_desc'] = "カスタムモデレータツールを使うと、スレッドにも記事にも使えるモデレータツールの組み合わせを作れます。デフォルトのツールと同様にフォーラムの管理に利用できます。ここではそのカスタムスレッドツールの管理ができます。";
$l['add_thread_tool'] = "スレッドツールの追加";
$l['add_new_thread_tool'] = "新しいスレッドツールの追加";
$l['add_thread_tool_desc'] = "ここではカスタムスレッドモデレーションツールを新しく追加できます。追加したツールは、モデレータ用クイックスレッドツールからシステムデフォルトのツールと同様に利用できます。";
$l['post_tools'] = "記事ツール";
$l['post_tools_desc'] = "カスタムモデレータツールを使うと、スレッドにも記事にも使えるモデレータツールの組み合わせを作れます。デフォルトのツールと同様にフォーラムの管理に利用できます。ここではそのカスタム記事ツールの管理ができます。";
$l['add_post_tool'] = "記事ツールを追加";
$l['add_new_post_tool'] = "新しい記事ツールを追加";
$l['add_post_tool_desc'] = "ここではカスタム記事モデレーションツールを新しく追加できます。追加したツールは、モデレータ用クイック記事ツールからシステムデフォルトのツールと同様に利用できます。";
$l['edit_post_tool'] = "記事ツールを編集";
$l['edit_post_tool_desc'] = "ここでは記事ツールの設定とアクションを編集できます。";
$l['edit_thread_tool'] = "スレッドツールを編集";
$l['edit_thread_tool_desc'] = "ここではスレッドツールの設定とアクションを編集できます。";
$l['no_thread_tools'] = "フォーラムに設定されたスレッドツールがありません。";
$l['no_post_tools'] = "フォーラムに設定された記事ツールがありません。";
$l['confirm_thread_tool_deletion'] = "本当にこのスレッドツールを削除しますか？";
$l['confirm_post_tool_deletion'] = "本当にこの記事ツールを削除しますか？";
$l['success_post_tool_deleted'] = "指定された記事モデレーションツールを削除しました。";
$l['success_thread_tool_deleted'] = "指定されたスレッドモデレーションツールを削除しました。";
$l['error_invalid_post_tool'] = "指定された記事ツールは存在しません。";
$l['error_invalid_thread_tool'] = "指定されたスレッドツールは存在しません。";
$l['general_options'] = "一般オプション";
$l['short_description'] = "簡単な説明";
$l['available_in_forums'] = "次のフォーラムで有効にする";
$l['all_forums'] = "すべてのフォーラム";
$l['select_forums'] = "選択したフォーラム（複数選択可）";
$l['save_thread_tool'] = "スレッドツールを保存";
$l['title'] = "タイトル";
$l['thread_moderation'] = "スレッドモデレーション";
$l['approve_unapprove'] = "スレッドを承認/却下しますか？";
$l['no_change'] = "変更しない";
$l['approve'] = "承認する";
$l['unapprove'] = "却下する";
$l['stick'] = "固定";
$l['unstick'] = "固定解除";
$l['open'] = "返信受付中に変更";
$l['close'] = "締め切りに変更";
$l['toggle'] = "トグル";
$l['days'] = "日間";
$l['no_prefix'] = "プリフィックスなし";
$l['forum_to_move_to'] = "移動先のフォーラム:";
$l['leave_redirect'] = "移動元から転送しますか？";
$l['delete_redirect_after'] = "転送期間";
$l['do_not_move_thread'] = "スレッドを移動しない";
$l['do_not_copy_thread'] = "スレッドをコピーしない";
$l['move_thread'] = "スレッドを移動しますか？";
$l['move_thread_desc'] = "スレッドを移動する場合、転送期間は転送する選択をした場合にのみ入力してください。";
$l['forum_to_copy_to'] = "コピー先のフォーラム:";
$l['copy_thread'] = "スレッドをコピーしますか？";
$l['open_close_thread'] = "返信受付中/締め切りの状態を変更しますか？";
$l['delete_thread'] = "スレッドを削除しますか？";
$l['merge_thread'] = "スレッドをマージしますか？";
$l['merge_thread_desc'] = "モデレータ用クイックツールで利用される場合に限り";
$l['delete_poll'] = "アンケートを削除しますか？";
$l['delete_redirects'] = "転送を削除しますか？";
$l['apply_thread_prefix'] = "スレッドプリフィックスを適用しますか？";
$l['new_subject'] = "新しく件名をつけ直しますか？";
$l['new_subject_desc'] = "{subject} はもとの件名、{username} はモデレータ名で置き換わります。";
$l['add_new_reply'] = "新しい返信を追加";
$l['add_new_reply_desc'] = "返信しない場合は空欄";
$l['reply_subject'] = "返信の件名";
$l['reply_subject_desc'] = "返信を作成した場合に限り必要です。<br />{subject} はオリジナルの件名で、{username} はモデレータ名で置き換わります。";
$l['success_mod_tool_created'] = "モデレーションツールを作成しました。";
$l['success_mod_tool_updated'] = "モデレーションツールを更新しました。";
$l['inline_post_moderation'] = "モデレータ用クイックツール";
$l['delete_posts'] = "記事を削除しますか？";
$l['merge_posts'] = "記事をマージしますか？";
$l['merge_posts_desc'] = "モデレータ用クイックツールから利用された場合にのみ使われます。";
$l['approve_unapprove_posts'] = "記事の承認状態を変更しますか？";
$l['split_posts'] = "記事を分割";
$l['split_posts2'] = "記事を分割しますか？";
$l['do_not_split'] = "記事を分割しない";
$l['split_to_same_forum'] = "同じフォーラム内に分割する";
$l['close_split_thread'] = "分割したスレッドを締め切りますか？";
$l['stick_split_thread'] = "分割したスレッドを固定しますか？";
$l['unapprove_split_thread'] = "分割したスレッドを却下しますか？";
$l['split_thread_subject'] = "分割したスレッドの件名";
$l['split_thread_subject_desc'] = "{subject} はもとの件名で置き換わります。スレッドを分割する場合に限り必要です。";
$l['add_new_split_reply'] = "分割したスレッドに返信を追加";
$l['add_new_split_reply_desc'] = "返信しない場合は空欄";
$l['split_reply_subject'] = "返信の件名";
$l['split_reply_subject_desc'] = "返信を作成する場合に限り必要です。";
$l['save_post_tool'] = "記事ツールを保存";
$l['error_missing_title'] = "このツールの名前を入力してください。";
$l['error_missing_description'] = "このツールの簡単な説明を入力してください。";
$l['error_no_forums_selected'] = "このツールを利用するフォーラムを選択してください。";
$l['error_forum_is_category'] = "カテゴリタイプのフォーラムは選択できません。";
?>