<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id$
 */
 
$l['file_verification'] = "ファイルの検証";
$l['checking'] = "チェック中...";
$l['file_verification_message'] = "この処理はMyBBのすべてのデフォルトファイルについて、それが正規のものであるか確認します。「はい」をクリックして処理を進めてください。<br /><small>この処理ではファイルが改造されているのか、本当に壊れているの違いを判断できません。したがって「壊れた」ファイルをもとに戻すときには十分注意してください。</small>";

$l['error_communication'] = "MyBBサーバとの通信中に問題が発生しました。しばらく待って再度お試しください。";
$l['file'] = "ファイル";
$l['no_corrupt_files_found'] = "壊れたファイルは見つかりませんでした。";
$l['found_problems'] = "問題あり";
$l['no_problems_found'] = "問題なし";
$l['changed'] = "変更あり";
$l['missing'] = "ファイルなし";
$l['status'] = "状態";

?>
