<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: user_titles.lang.php 5349 2011-02-14 23:51:52Z MattR $
 */

$l['user_titles'] = "ユーザタイトル";
$l['user_titles_desc'] = "ここではユーザのタイトルを管理できます。タイトルとはユーザの投稿数に応じて割り当てられるタイトルで、投稿数に応じた星の数とともに表示できます。";
$l['add_new_user_title'] = "新しいタイトルを追加";
$l['add_new_user_title_desc'] = "ここでは新しいタイトルを作成できます。
<i>注意: ユーザタイトルは<u><a href=\"index.php?module=user-group_promotions\">グループ昇格</a></u>とは異なるシステムです。</i>";
$l['error_missing_title'] = "ユーザのタイトルが入力されていません。";
$l['error_missing_posts'] = "このタイトルのための最低投稿数が入力されていません。";
$l['error_cannot_have_same_posts'] = "最低投稿数に別にタイトルの最低投稿数と同じ数字を指定することはできません。";
$l['error_invalid_user_title'] = "指定されたタイトルが間違っています。";
$l['success_user_title_created'] = "新しいタイトルを作成しました。";
$l['success_user_title_updated'] = "タイトルを更新しました。";
$l['success_user_title_deleted'] = "指定されたタイトルを削除しました。";
$l['title_to_assign'] = "割り当てるタイトル";
$l['title_to_assign_desc'] = "このタイトルは、カスタムタイトルが設定されていなければ、ユーザ名の下に表示されます。";
$l['minimum_posts'] = "最低投稿数";
$l['minimum_posts_desc'] = "このタイトルを与えるために必要な最低投稿数。";
$l['number_of_stars'] = "星の数";
$l['number_of_stars_desc'] = "タイトルの下に表示する星の数を入力してください。0を指定すると星を表示しません。";
$l['star_image'] = "星の画像";
$l['star_image_desc'] = "タイトルと一緒に表示する星をカスタマイズする場合に、使用する星の画像へのパスを入力します。空欄の場合には、グループの星画像が使用されます。{theme}は現在のテーマディレクトリと置き換わります。";
$l['save_user_title'] = "タイトルを保存";
$l['edit_user_title'] = "タイトルを編集";
$l['edit_user_title_desc'] = "ここではタイトルを編集できます。";
$l['user_title_deletion_confirmation'] = "本当にこのタイトルを削除しますか？";
$l['manage_user_titles'] = "タイトルの管理";
$l['user_title'] = "タイトル";
$l['no_user_titles'] = "現在、定義されているタイトルがありません。";
?>
