<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_attachment_types.lang.php 5833 2012-05-24 08:39:00Z Tomm $
 */

$l['attachment_types'] = "添付ファイルタイプ";
$l['attachment_types_desc'] = "ここでは、ユーザが記事に添付できるファイルのタイプを制限するため、ファイルタイプを作成/管理することができます。";
$l['add_new_attachment_type'] = "添付ファイルタイプを追加";
$l['add_attachment_type'] = "添付ファイルタイプを追加";
$l['add_attachment_type_desc'] = "新しく添付ファイルタイプを追加すると、そのタイプのファイルを記事に添付できるようになります。タイプごとに拡張子、MIMEタイプ、最大サイズ、およびアイコンを設定できます。";
$l['edit_attachment_type'] = "添付ファイルタイプを編集";
$l['edit_attachment_type_desc'] = "拡張子、MIMEタイプ、最大サイズ、およびアイコンを設定できます。";
$l['extension'] = "拡張子";
$l['maximum_size'] = "最大サイズ";
$l['no_attachment_types'] = "現在、添付ファイルタイプが設定されていません。";
$l['file_extension'] = "ファイル拡張子";
$l['file_extension_desc'] = "アップロードを許可するファイル拡張子を入力してください。拡張子の前のピリオドを不要です。 (例: txt)";
$l['mime_type'] = "MIMEタイプ";
$l['mime_type_desc'] = "このファイルをダウンロードするときにサーバが送信するMIMEタイプを入力してください。※<a href=\"http://www.webmaster-toolkit.com/mime-types.shtml\">MIMEタイプの一覧はこちら</a>";
$l['maximum_file_size'] = "MAXファイルサイズ (KB)";
$l['maximum_file_size_desc'] = "この添付ファイルタイプでアップロードできる最大サイズをKB単位で指定 (1MB = 1024KB)";
$l['limit_intro'] = "最大ファイルサイズはPHPにより制限される次の値より小さな値を設定してください。";
$l['limit_post_max_size'] = "記事の最大サイズ: {1}";
$l['limit_upload_max_filesize'] = "アップロード可能な最大ファイルサイズ: {1}";
$l['attachment_icon'] = "添付ファイルアイコン";
$l['attachment_icon_desc'] = "このタイプを表すアイコンがあれば、画像ファイルへのパスを入力してください。{theme}はユーザの使用するテーマの画像ディレクトリに置き換えられ、テーマごとにアイコンを指定できるようになります。";
$l['save_attachment_type'] = "添付ファイルタイプを保存";
$l['error_invalid_attachment_type'] = "選択された添付ファイルタイプは正しくありません。";
$l['error_missing_mime_type'] = "添付ファイルタイプに対応するMIMEタイプを入力してください。";
$l['error_missing_extension'] = "このファイルタイプに対応するファイル拡張子を入力してください。";
$l['success_attachment_type_created'] = "添付ファイルタイプは正常に作成されました。";
$l['success_attachment_type_updated'] = "添付ファイルタイプは正常に更新されました。";
$l['success_attachment_type_deleted'] = "添付ファイルタイプは正常に削除されました。";
$l['confirm_attachment_type_deletion'] = "本当にこのファイルタイプを削除しますか？";
?>
