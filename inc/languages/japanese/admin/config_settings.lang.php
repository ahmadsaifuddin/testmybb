<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 *
 * $Id$
 */

$l['board_settings'] = "掲示板の設定";
$l['change_settings'] = "設定を変更";
$l['change_settings_desc'] = "ここでは掲示板に関するさまざまな設定を管理できます。設定はグループごとにまとめられています。設定を変更するには、グループを選択してください。";
$l['add_new_setting'] = "新しい設定項目の追加";
$l['add_new_setting_desc'] = "ここでは掲示板の新しい設定項目を追加できます。";
$l['modify_existing_settings'] = "設定項目の編集";
$l['modify_existing_settings_desc'] = "ここでは既存の設定項目の編集ができます。";
$l['add_new_setting_group'] = "新しい設定グループの追加";
$l['add_new_setting_group_desc'] = "ここでは設定項目を分類するための設定グループを新しく作ることができます。";
$l['edit_setting_group'] = "設定グループの編集";
$l['edit_setting_group_desc'] = "ここでは既存の設定グループの編集ができます。";
$l['title'] = "タイトル";
$l['description'] = "説明";
$l['group'] = "グループ";
$l['display_order'] = "表示順";
$l['name'] = "識別子";
$l['name_desc'] = "このユニークな識別子は、この設定項目を（スクリプト、言語パック、テンプレートなどから）参照するために使われます。";
$l['group_name_desc'] = "このユニークな識別子は言語パックで使用されます。";
$l['text'] = "テキスト";
$l['textarea'] = "テキストエリア";
$l['yesno'] = "はい / いいえ の選択";
$l['onoff'] = "On / Off の選択";
$l['select'] = "選択ボックス";
$l['radio'] = "ラジオボタン";
$l['checkbox'] = "チェックボックス";
$l['language_selection_box'] = "言語選択ボックス";
$l['adminlanguage'] = "管理者用言語選択ボックス";
$l['cpstyle'] = "コントロールパネルスタイル選択ボックス";
$l['php'] = "PHPコード";
$l['type'] = "タイプ";
$l['extra'] = "追加情報";
$l['extra_desc'] = "この項目が選択ボックス、ラジオボタン、またはチェックボックスの場合には、表示する項目をキーとアイテムを一組にして（キー=アイテム）入力してください。アイテムごとに改行してください。PHPの場合には、評価するPHPのコードを入力してください。";
$l['value'] = "値";
$l['insert_new_setting'] = "新しい設定を挿入";
$l['edit_setting'] = "設定を編集";
$l['delete_setting'] = "設定を削除";
$l['setting_configuration'] = "設定内容の編集";
$l['update_setting'] = "設定を更新";
$l['save_settings'] = "設定を保存";
$l['setting_groups'] = "設定グループ";
$l['bbsettings'] = "項目";
$l['insert_new_setting_group'] = "新しい設定グループを挿入";
$l['setting_group_setting'] = "設定グループ /　設定";
$l['order'] = "順序";
$l['delete_setting_group'] = "設定グループを削除";
$l['save_display_orders'] = "表示順を保存";
$l['update_setting_group'] = "設定グループを更新";
$l['modify_setting'] = "設定項目の編集";
$l['search'] = "検索";
$l['show_all_settings'] = "設定をすべて表示";
$l['settings_search'] = "設定を検索";
$l['confirm_setting_group_deletion'] = "本当にこの設定グループを削除しますか？";
$l['confirm_setting_deletion'] = "本当にこの設定を削除しますか？";
$l['error_missing_title'] = "設定のタイトルが入力されていません。";
$l['error_missing_group_title'] = "設定グループのタイトルが入力されていません。";
$l['error_invalid_gid'] = "設定を配置するグループが正しくありません。";
$l['error_invalid_gid2'] = "設定グループへのリンクが間違っています。存在するリンクを指定してください。";
$l['error_missing_name'] = "設定の識別子が入力されていません。";
$l['error_missing_group_name'] = "設定グループの識別子が入力されていません。";
$l['error_invalid_type'] = "選択された設定のタイプが間違っています。";
$l['error_invalid_sid'] = "指定された設定は存在しません。";
$l['error_duplicate_name'] = "指定された識別子はすでに「{1}」設定に使用済みです。ユニークな識別子を指定してください。";
$l['error_duplicate_group_name'] = "指定された識別子はすでに「{1}」設定グループに使用済みです。ユニークな識別子を指定してください。";
$l['error_no_settings_found'] = "指定された検索条件に一致する設定が見つかりませんでした。";
$l['error_cannot_edit_default'] = "デフォルトの設定や設定グループは編集したり削除することはできません。";
$l['error_cannot_edit_php'] = "編集できない特別なタイプの設定です。";
$l['error_ajax_search'] = "設定の検索で次の問題が起きました:";
$l['error_ajax_unknown'] = "設定の検索中に未知のエラーが発生しました。";
$l['error_chmod_settings_file'] = "設定ファイル「./inc/settings.php」に書き込めません。CHMODで777に設定してください。<br />CHMODの詳しい説明については<a href=\"http://docs.mybb.com/HowTo_Chmod.html\" target=\"_blank\">MyBB Docs</a>をご覧ください。";
$l['success_setting_added'] = "設定は正常に作成されました。";
$l['success_setting_updated'] = "設定は正常に更新されました。";
$l['success_setting_deleted'] = "設定は正常に削除されました。";
$l['success_settings_updated'] = "設定は正常に更新されました。";
$l['success_display_orders_updated'] = "設定の表示順は正常に更新されました。";
$l['success_setting_group_added'] = "設定グループは正常に作成されました。";
$l['success_setting_group_updated'] = "設定グループは正常に削除されました。";
$l['success_setting_group_deleted'] = "選択した設定グループは正常に削除されました。";
$l['success_duplicate_settings_deleted'] = "重複した設定グループはすべて正常に削除されました。";
?>