<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_post_icons.lang.php 5833 2012-05-24 08:39:00Z Tomm $
 */

$l['post_icons'] = "記事アイコン";
$l['add_post_icon'] = "新しい記事アイコンを追加";
$l['add_post_icon_desc'] = "ここでは新しい記事アイコンをひとつずつ追加できます。";
$l['add_multiple_post_icons'] = "記事アイコンをまとめて追加";
$l['add_multiple_post_icons_desc'] = "ここでは新しい記事アイコンをまとめて追加できます。";
$l['edit_post_icon'] = "記事アイコンを編集";
$l['edit_post_icon_desc'] = "ここで記事アイコンを編集できます。";
$l['manage_post_icons'] = "記事アイコンを管理";
$l['manage_post_icons_desc'] = "ここでは記事アイコンを編集したり削除したりといった管理ができます。";
$l['name_desc'] = "記事アイコンの名前";
$l['image_path'] = "画像へのパス";
$l['image_path_desc'] = "記事アイコンの画像へのパス";
$l['save_post_icon'] = "記事アイコンを保存";
$l['reset'] = "リセット";
$l['path_to_images'] = "画像へのパス";
$l['path_to_images_desc'] = "画像が保存されているフォルダへのパス";
$l['show_post_icons'] = "記事アイコンを見る";
$l['image'] = "画像";
$l['add'] = "追加しますか？";
$l['save_post_icons'] = "記事アイコンを保存";
$l['no_post_icons'] = "現在、フォーラムには記事アイコンがありません。";
$l['error_missing_name'] = "記事アイコンの名前が入力されていません。";
$l['error_missing_path'] = "記事アイコンへのパスが入力されていません。";
$l['error_missing_path_multiple'] = "パスが入力されていません。";
$l['error_invalid_path'] = "入力されたパスが間違っています。";
$l['error_no_images'] = "指定されたディレクトリに記事アイコンが存在しません。またはそのディレクトリ下のすべての記事アイコンはすでに登録済みです。";
$l['error_none_included'] = "取り込む記事アイコンが選択されていません。";
$l['error_invalid_post_icon'] = "指定された記事アイコンが存在しません。";
$l['success_post_icon_added'] = "記事アイコンを追加しました。";
$l['success_post_icons_added'] = "選択された記事アイコンを追加しました。";
$l['success_post_icon_updated'] = "記事アイコンを更新しました。";
$l['success_post_icon_deleted'] = "選択された記事アイコンを削除しました。";
$l['confirm_post_icon_deletion'] = "本当にこの記事アイコンを削除しますか？";
?>