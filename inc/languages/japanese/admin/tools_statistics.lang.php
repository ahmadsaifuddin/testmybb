<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: tools_statistics.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['statistics'] = "統計情報";
$l['overall_statistics'] = "全体の統計";
$l['overall_statistics_desc'] = "ここでは掲示板全体の統計が見られます。時刻はすべてUTCです。";

$l['date_range'] = "表示する期間";

$l['date'] = "日付";
$l['users'] = "ユーザ数";
$l['threads'] = "スレッド数";
$l['posts'] = "記事数";

$l['from'] = "開始日";
$l['to'] = "&nbsp;終了日";

$l['increase'] = "増加";
$l['no_change'] = "変化なし";
$l['decrease'] = "減少";

$l['error_no_results_found_for_criteria'] = "指定された期間のデータがありません。別の期間を指定してください。";
$l['error_no_statistics_available_yet'] = "まだ統計データがありません。";
?>
