<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_module_meta.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */
 
$l['configuration'] = "設定";
$l['bbsettings'] = "掲示板の設定";
$l['banning'] = "追放 / 禁止";
$l['custom_profile_fields'] = "カスタムプロフィール項目";
$l['smilies'] = "スマイリー";
$l['word_filters'] = "NGワード";
$l['mycode'] = "MyCode";
$l['languages'] = "言語パック";
$l['post_icons'] = "記事アイコン";
$l['help_documents'] = "ヘルプ";
$l['plugins'] = "プラグイン";
$l['attachment_types'] = "添付ファイルタイプ";
$l['moderator_tools'] = "モデレータツール";
$l['spiders_bots'] = "クローラ/Bot";
$l['calendars'] = "カレンダー";
$l['warning_system'] = "警告システム";
$l['thread_prefixes'] = "スレッドプリフィックス";
$l['can_manage_settings'] = "設定の管理を許可しますか？";
$l['can_manage_banned_accounts'] = "追放アカウントの管理を許可しますか？";
$l['can_manage_custom_profile_fields'] = "カスタムプロフィール項目の管理を許可しますか？";
$l['can_manage_smilies'] = "スマイリーの管理を許可しますか？";
$l['can_manage_bad_words'] = "NGワードの管理を許可しますか？";
$l['can_manage_custom_mycode'] = "カスタムMyCodeの管理を許可しますか？";
$l['can_manage_language_packs'] = "言語パックの管理を許可しますか？";
$l['can_manage_post_icons'] = "記事アイコンの管理を許可しますか？";
$l['can_manage_help_documents'] = "ヘルプの管理を許可しますか？";
$l['can_manage_plugins'] = "プラグインの管理を許可しますか？";
$l['can_manage_attachment_types'] = "添付ファイルタイプの管理を許可しますか？";
$l['can_manage_spiders_bots'] = "クローラ/Botの管理を許可しますか？";
$l['can_manage_calendars'] = "カレンダーの管理を許可しますか？";
$l['can_manage_warning_system'] = "警告システムの管理を許可しますか？";
$l['can_manage_mod_tools'] = "モデレートツールの管理を許可しますか？";
$l['can_manage_thread_prefixes'] = "スレッドプリフィックスの管理を許可しますか？";
?>