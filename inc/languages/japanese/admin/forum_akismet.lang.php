<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: forum_akismet.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['akismet'] = "Akismet";
$l['akismet_desc'] = "Akismetはフォーラムをスパムから守る手助けをするためのプラグラムです。";
$l['can_manage_akismet'] = "Akismetの管理を許可";
$l['title'] = "タイトル";
$l['detected_spam_messages'] = "検出されたスパム";
$l['unmark_selected'] = "選択したスパム判定を解除";
$l['deleted_selected'] = "選択したメッセージを削除";
$l['delete_all'] = "すべて削除";
$l['success_deleted_spam'] = "検出されたスパムをすべて削除しました。";
$l['success_unmarked'] = "選択されたメッセージのスパム判定を解除しました。";
$l['success_spam_deleted'] = "選択されたスパムを削除しました。";
$l['confirm_spam_deletion'] = "本当に検出されたメッセージをすべて削除しますか？";
$l['error_deletepost'] = "削除する記事を選択してください。";
$l['error_unmark'] = "解除する記事を選択してください。";
$l['no_spam_found'] = "現在、Akismetがスパムと判定したメッセージはありません。";
?>