<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: forum_moderation_queue.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

// Tabs
$l['moderation_queue'] = "モデレーションキュー";
$l['threads'] = "スレッド";
$l['threads_desc'] = "ここではモデレート待ちのスレッドを表示して承認できます。";
$l['posts'] = "投稿";
$l['posts_desc'] = "ここではモデレート待ちの記事を表示して承認できます。";
$l['attachments'] = "添付ファイル";
$l['attachments_desc'] = "ここではモデレート待ちの添付ファイルを表示して承認できます。";
$l['threads_awaiting_moderation'] = "モデレート待ちのスレッド";
$l['posts_awaiting_moderation'] = "モデレート待ちの記事";
$l['attachments_awaiting_moderation'] = "モデレート待ちの添付ファイル";
// Errors
$l['error_no_posts'] = "現在、モデレート待ちの記事はありません。";
$l['error_no_attachments'] = "現在、モデレート待ちの添付ファイルはありません。";
$l['error_no_threads'] = "現在、モデレート待ちのスレッド、記事、添付ファイルはありません。";
// Success
$l['success_threads'] = "選択されたスレッドをモデレートしました。";
$l['success_posts'] = "選択された記事をモデレートしました。";
$l['success_attachments'] = "選択された添付ファイルをモデレートしました。";
// Pages
$l['subject'] = "件名";
$l['author'] = "投稿者";
$l['posted'] = "投稿日";
$l['ignore'] = "無視";
$l['approve'] = "承認";
$l['forum'] = "フォーラム:";
$l['thread'] = "スレッド:";
$l['post'] = "記事:";
$l['re'] = "RE:";
$l['filename'] = "ファイル名";
$l['uploadedby'] = "アップロードしたユーザ";
$l['controls'] = "制御";
// Buttons
$l['mark_as_ignored'] = "すべて無視する";
$l['mark_as_deleted'] = "すべて削除する";
$l['mark_as_approved'] = "すべて承認する";
$l['perform_action'] = "アクションを実行";
?>