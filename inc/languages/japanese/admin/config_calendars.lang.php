<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_calendars.lang.php 5833 2012-05-24 08:39:00Z Tomm $
 */

$l['calendars'] = "カレンダー";
$l['manage_calendars'] = "カレンダーの管理";
$l['manage_calendars_desc'] = "ここでは掲示板上のカレンダーを管理できます。カレンダーの表示順を変更した場合には、ページの下部にある保存ボタンをクリックしてください。";
$l['add_calendar'] = "新しいカレンダーを追加";
$l['add_calendar_desc'] = "新しいカレンダーを作成できます。";
$l['edit_calendar'] = "カレンダーを編集";
$l['edit_calendar_desc'] = "ここではカレンダーの設定を編集できます。";
$l['calendar'] = "カレンダー";
$l['order'] = "表示順";
$l['no_calendars'] = "現在、カレンダーがありません。";
$l['save_calendar_orders'] = "カレンダーの表示順を保存";
$l['name'] = "名前";
$l['display_order'] = "表示順";
$l['week_start'] = "週の開始";
$l['week_start_desc'] = "週の開始を何曜日にするか設定してください。";
$l['sunday'] = "日曜日";
$l['monday'] = "月曜日";
$l['tuesday'] = "火曜日";
$l['wednesday'] = "水曜日";
$l['thursday'] = "木曜日";
$l['friday'] = "金曜日";
$l['saturday'] = "土曜日";
$l['event_limit'] = "イベント表示数の上限";
$l['event_limit_desc'] = "ここで指定した数より1日に登録されたイベント数が多くなると、すべてのイベントをまとめて表示するリンクが代わりに表示されます。";
$l['show_birthdays'] = "誕生日を表示しますか？";
$l['show_birthdays_desc'] = "登録ユーザの誕生日をカレンダーに表示しますか？";
$l['moderate_events'] = "新しいイベントをモデレートしますか？";
$l['moderate_events_desc'] = "「はい」に設定すると「モデレートを省略」設定にしてあるメンバー以外すべてのイベントにモデレートが必要になります。";
$l['allow_html'] = "イベントにHTMLを使用を許可しますか？";
$l['allow_mycode'] = "イベントにMyCodeの使用を許可しますか？";
$l['allow_img'] = "イベントに [IMG] コードの使用を許可しますか？";
$l['allow_video'] = "イベントに [VIDEO] コードの使用を許可しますか？";
$l['allow_smilies'] = "イベントにスマイリーの使用を許可しますか？";
$l['save_calendar'] = "カレンダーを保存";
$l['permissions'] = "権限";
$l['edit_permissions'] = "カレンダーの権限を編集";
$l['calendar_permissions_for'] = "カレンダー権限の設定:";
$l['permissions_group'] = "グループ";
$l['permissions_view'] = "表示";
$l['permissions_post_events'] = "イベントを作成";
$l['permissions_bypass_moderation'] = "モデレートを省略";
$l['permissions_moderator'] = "モデレータ権限";
$l['permissions_all'] = "全権限";
$l['permissions_use_group_default'] = "グループのデフォルトを使用";
$l['save_permissions'] = "権限を保存";
$l['error_invalid_calendar'] = "指定されたカレンダーは存在しません。";
$l['error_missing_name'] = "カレンダーの名前が入力されていません。";
$l['error_missing_order'] = "カレンダーの表示順が入力されていません。";
$l['success_calendar_created'] = "カレンダーは正常に作成されました。";
$l['success_calendar_updated'] = "カレンダーは正常に更新されました。";
$l['success_calendar_permissions_updated'] = "カレンダー権限は正常に更新されました。";
$l['success_calendar_deleted'] = "選択したカレンダーは正常に削除されました。";
$l['success_calendar_orders_updated'] = "カレンダーの表示順は正常に更新されました。";
$l['confirm_calendar_deletion'] = "本当にこのカレンダーを削除しますか？";
?>
