<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id$
 */
 
$l['custom_profile_fields'] = "カスタムプロフィール項目";
$l['custom_profile_fields_desc'] = "ここではプロフィールのカスタム項目を編集したり削除したりといった管理ができます。";
$l['add_profile_field'] = "プロフィール項目を追加";
$l['add_new_profile_field'] = "新しいプロフィール項目を追加";
$l['add_new_profile_field_desc'] = "ここではプロフィールのカスタム項目を新規追加できます。";
$l['edit_profile_field'] = "プロフィール項目を編集";
$l['edit_profile_field_desc'] = "ここではプロフィールのカスタム項目を編集できます。";
$l['title'] = "タイトル";
$l['short_description'] = "簡単な説明";
$l['maximum_length'] = "最大長";
$l['maximum_length_desc'] = "入力可能な文字数の上限。テキストボックスまたはテキストエリアに適用されます。";
$l['field_length'] = "フィールド長";
$l['field_length_desc'] = "フィールドの長さ。選択ボックスに適用されます。";
$l['display_order'] = "表示順";
$l['display_order_desc'] = "カスタムプロフィール項目の表示順です。必ず他のプロフィール項目と異なる番号を割り当ててください。";
$l['text'] = "テキストボックス";
$l['textarea'] = "テキストエリア";
$l['select'] = "選択ボックス";
$l['multiselect'] = "複数選択可能な選択ボックス";
$l['radio'] = "ラジオボタン";
$l['checkbox'] = "チェックボックス";
$l['field_type'] = "フィールドタイプ";
$l['field_type_desc'] = "表示するフィールドのタイプを指定します。";
$l['selectable_options'] = "選択可能なオプションは？";
$l['selectable_options_desc'] = "オプションを1行ずつに区切って入力してください。選択ボックス、チェックボックス、ラジオボタンに適用されます。";
$l['required'] = "必須項目にしますか？";
$l['required_desc'] = "登録時やプロフィール編集時に入力必須の項目としますか？フィールドが非表示だったりユーザに編集を許可していない場合には必須項目とできないので注意してください。";
$l['editable_by_user'] = "ユーザに編集を許可しますか？";
$l['editable_by_user_desc'] = "この項目をユーザが編集することを許可しますか？許可しない場合でも、管理者やモデレータはこの項目を編集可能です。";
$l['hide_on_profile'] = "プロフィール上で非表示としますか？";
$l['hide_on_profile_desc'] = "ユーザのプロフィール上で非表示としますか？非表示の場合、管理者またはモデレータだけが見ることができます。";
$l['min_posts_enabled'] = "最低記事数を設定しますか？";
$l['min_posts_enabled_desc'] = "この項目を一定数以上の記事を投稿したユーザにだけ適用しますか？最低記事数を設定する場合には、ここにその数を入力してください。";
$l['save_profile_field'] = "プロフィール項目を保存";
$l['name'] = "名前";
$l['id'] = "ID";
$l['editable'] = "編集可能にしますか？";
$l['hidden'] = "非表示にしますか？";
$l['edit_field'] = "項目を編集";
$l['delete_field'] = "項目を削除";
$l['no_profile_fields'] = "現在、フォーラムにカスタムプロフィール項目はありません。";
$l['error_missing_name'] = "カスタムプロフィール項目のタイトルが入力されていません。";
$l['error_missing_description'] = "カスタムプロフィール項目の説明が入力されていません。";
$l['error_missing_filetype'] = "カスタムプロフィール項目のフィールドタイプが入力されていません。";
$l['error_missing_required'] = "必須項目にするかどうかが選択されていません。";
$l['error_missing_editable'] = "ユーザに編集を許可するかどうかが選択されていません。";
$l['error_missing_hidden'] = "プロフィール上で非表示にするかどうかが選択されていません。";
$l['error_invalid_fid'] = "選択されたプロフィール項目は存在しません。";
$l['success_profile_field_added'] = "カスタムプロフィール項目が追加されました。";
$l['success_profile_field_saved'] = "カスタムプロフィール項目が保存されました。";
$l['success_profile_field_deleted'] = "カスタムプロフィール項目が削除されました。";
$l['confirm_profile_field_deletion'] = "本当にこのカスタムプロフィール項目を削除しますか？";
?>