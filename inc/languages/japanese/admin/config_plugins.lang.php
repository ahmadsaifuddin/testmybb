<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_plugins.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['plugins'] = "プラグイン";
$l['plugins_desc'] = "ここではフォーラムの<strong>inc/plugins</strong>にアップロードされているプラグインに対し、有効化、無効化などの管理ができます。無効化すると、プラグイン自体の情報は何も失われずに見えなくなります。";
$l['plugin_updates'] = "プラグインの更新";
$l['plugin_updates_desc'] = "ここではインストール済みのプラグインのアップデートを確認できます。";
$l['browse_plugins'] = "プラグインを探す";
$l['browse_plugins_desc'] = "ここではMyBB公式プラグインサイトを表示して互換なプラグインを探すことができます。";
$l['browse_all_plugins'] = "すべてのプラグインから探す";
$l['plugin'] = "プラグイン";
$l['your_version'] = "現在のバージョン";
$l['latest_version'] = "最新のバージョン";
$l['download'] = "ダウンロード";
$l['deactivate'] = "有効化";
$l['activate'] = "無効化";
$l['install_and_activate'] = "インストール &amp; 有効化";
$l['uninstall'] = "アンインストール";
$l['created_by'] = "作者";
$l['no_plugins'] = "現在プラグインはインストールされていません。";
$l['plugin_incompatible'] = "このプラグインはMyBB{1}と互換性がありません。";
$l['recommended_plugins_for_mybb'] = "MyBB{1}にお勧めのプラグイン";
$l['browse_results_for_mybb'] = "Browse Results for MyBB {1}";
$l['search_for_plugins'] = "プラグインを検索";
$l['search'] = "検索";
$l['error_vcheck_no_supported_plugins'] = "バージョンチェックをサポートしているプラグインがインストールされていません。";
$l['error_vcheck_communications_problem'] = "MyBBバージョンサーバとの通信で問題が発生しました。しばらく時間を置いてから再度お試しください。";
$l['error_no_input'] = "エラーコード1: 入力がありません。";
$l['error_no_pids'] = "エラーコード2: プラグインIDが指定されていません。";
$l['error_communication_problem'] = "MyBBバージョンサーバとの通信で問題が発生しました。しばらく時間を置いてから再度お試しください。";
$l['error_invalid_plugin'] = "選択したプラグインが存在しません。";
$l['error_no_results_found'] = "指定されたキーワードに該当する結果が見つかりませんでした。";
$l['success_plugins_up_to_date'] = "すべてのプラグインは最新です。";
$l['success_plugin_activated'] = "選択されたプラグインを有効にしました。";
$l['success_plugin_deactivated'] = "選択されたプラグインを無効にしました。";
$l['success_plugin_installed'] = "選択されたプラグインをインストールして有効化しました。";
$l['success_plugin_uninstalled'] = "選択されたプラグインをアンインストールしました。";
?>