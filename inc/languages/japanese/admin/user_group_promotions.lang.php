<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: user_group_promotions.lang.php 5746 2012-02-03 10:03:25Z Tomm $
 */

$l['user_group_promotions'] = "ユーザグループ昇格";
$l['user_group_promotions_desc'] = "ここではユーザグループの昇格を管理できます。";
$l['edit_promotion'] = "昇格を編集";
$l['edit_promotion_desc'] = "ここでは掲示板で自動的に行われる昇格を編集できます。";
$l['add_new_promotion'] = "新しい昇格を追加";
$l['add_new_promotion_desc'] = "ここでは掲示板で自動的に行われる昇格を新しく作成できます。";
$l['title'] = "タイトル";
$l['short_desc'] = "説明";
$l['post_count'] = "投稿数";
$l['reputation'] = "評価";
$l['referrals'] = "紹介者";
$l['time_registered'] = "登録期間";
$l['promo_requirements'] = "昇格の条件";
$l['promo_requirements_desc'] = "この昇格に必要とする条件を選択してください。複数選択したい場合には、CTRLキーを押しながら選択してください。";
$l['greater_than_or_equal_to'] = "以上";
$l['greater_than'] = "より多い";
$l['equal_to'] = "ちょうど";
$l['less_than_or_equal_to'] = "以下";
$l['less_than'] = "より少ない";
$l['reputation_count'] = "評価値";
$l['reputation_count_desc'] = "条件とする評価値を入力してください。評価値を条件とする場合には、数字を入力して比較条件をリストから選択してください。";
$l['referral_count'] = "紹介数";
$l['referral_count_desc'] = "条件とする紹介数を入力してください。紹介数を条件とする場合には、数字を入力して比較条件をリストから選択してください。";
$l['post_count_desc'] = "条件とする投稿数を入力してください。投稿数を条件とする場合には、数字を入力して比較条件をリストから選択してください。";
$l['hours'] = "時間";
$l['days'] = "日間";
$l['weeks'] = "週間";
$l['months'] = "か月";
$l['years'] = "年間";
$l['time_registered_desc'] = "昇格の条件に登録してから経過した時間を指定する場合には、数字を入力して単位をリストから選択してください。";
$l['all_user_groups'] = "すべてのユーザグループ";
$l['orig_user_group'] = "もとのユーザグループ";
$l['orig_user_group_desc'] = "昇格の対象とするユーザグループを選択してください。複数選択したい場合には、CTRLキーを押しながら選択してください。";
$l['new_user_group'] = "新しいユーザグループ";
$l['new_user_group_desc'] = "昇格後にユーザを移動する先のユーザグループを選択してください。";
$l['primary_user_group'] = "プライマリユーザグループ";
$l['secondary_user_group'] = "セカンダリユーザグループ";
$l['user_group_change_type'] = "ユーザグループ変更タイプ";
$l['user_group_change_type_desc'] = "移動先のグループを昇格したユーザのプライマリグループとする場合には「プライマリユーザグループ」を選択してください。新しいユーザグループをプロフィール上セカンダリグループとする場合には「セカンダリユーザグループ」を選択してください。";
$l['enabled'] = "有効にする";
$l['enable_logging'] = "ログに残す";
$l['promotion_logs'] = "昇格ログ";
$l['view_promotion_logs'] = "昇格ログを見る";
$l['view_promotion_logs_desc'] = "ここでは以前適用された昇格のログを閲覧できます。";
$l['promoted_user'] = "昇格したユーザ";
$l['time_promoted'] = "昇格した日時";
$l['no_promotion_logs'] = "現在、昇格の記録がありません。";
$l['promotion_manager'] = "昇格マネージャ";
$l['promotion'] = "昇格";
$l['disable_promotion'] = "昇格を無効";
$l['enable_promotion'] = "昇格を有効";
$l['delete_promotion'] = "昇格を削除";
$l['no_promotions_set'] = "現在、設定されている昇格はありません。";
$l['update_promotion'] = "昇格を保存";
$l['multiple_usergroups'] = "複数のユーザグループ";
$l['secondary'] = "セカンダリ";
$l['primary'] = "プライマリ";
$l['error_no_promo_id'] = "昇格IDが入力されていません。";
$l['error_invalid_promo_id'] = "入力された昇格IDが間違っています。";
$l['error_no_title'] = "昇格のタイトルが入力されていません。";
$l['error_no_desc'] = "昇格の説明が入力されていません。";
$l['error_no_requirements'] = "昇格の条件がひとつも選択されていません。";
$l['error_no_orig_usergroup'] = "昇格対象とするグループが選択されていません。";
$l['error_no_new_usergroup'] = "昇格後の移動先グループが選択されていません。";
$l['error_no_usergroup_change_type'] = "ユーザグループ変更タイプが選択されていません。";
$l['success_promo_disabled'] = "選択されたグループ昇格を無効にしました。";
$l['success_promo_deleted'] = "選択されたグループ昇格を削除しました。";
$l['success_promo_enabled'] = "選択されたグループ昇格を有効にしました。";
$l['success_promo_updated'] = "選択されたグループ昇格を更新しました。";
$l['success_promo_added'] = "グループ昇格を作成しました。";
$l['confirm_promo_disable'] = "本当にこの昇格を無効にしますか？";
$l['confirm_promo_deletion'] = "本当にこの昇格を削除しますか？";
?>