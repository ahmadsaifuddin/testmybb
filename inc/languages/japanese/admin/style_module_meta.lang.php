<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: style_module_meta.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */
 
$l['templates_and_style'] = "テンプレート &amp; スタイル";
$l['themes'] = "テーマ";
$l['templates'] = "テンプレート";
$l['can_manage_themes'] = "テーマの管理";
$l['can_manage_templates'] = "テンプレートの管理";
?>