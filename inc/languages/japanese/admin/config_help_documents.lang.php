<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_help_documents.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['help_documents'] = "ヘルプ";
$l['add_new_section'] = "新しい章の追加";
$l['add_new_section_desc'] = "ここでヘルプに新しい章を追加できます。";
$l['add_new_document'] = "新しいドキュメントの追加";
$l['add_new_document_desc'] = "ここでヘルプに新しいドキュメントを追加できます。";
$l['edit_section'] = "章を編集";
$l['edit_section_desc'] = "ここでヘルプの章を編集できます。";
$l['edit_document'] = "ドキュメントを編集";
$l['edit_document_desc'] = "ここでヘルプのドキュメントを編集できます。";
$l['manage_help_documents'] = "ヘルプの管理";
$l['manage_help_documents_desc'] = "ここではヘルプを管理できます。";
$l['title'] = "タイトル";
$l['short_description'] = "説明";
$l['display_order'] = "表示順";
$l['enabled'] = "有効にしますか？";
$l['use_translation'] = "翻訳を利用しますか？";
$l['add_section'] = "セクションを追加";
$l['add_document'] = "ドキュメントを追加";
$l['save_section'] = "セクションを保存";
$l['save_document'] = "ドキュメントを保存";
$l['section'] = "章";
$l['document'] = "ドキュメント本文";
$l['id'] = "ID";
$l['custom_doc_sec'] = "カスタムなドキュメント/章";
$l['default_doc_sec'] = "デフォルトのドキュメント/章";
$l['no_help_documents'] = "現在、ヘルプがありません。";
$l['section_document'] = "章 / ドキュメント";
$l['error_section_missing_name'] = "章の名前を入力してください。";
$l['error_section_missing_description'] = "章の説明を入力してください。";
$l['error_section_missing_enabled'] = "有効にするかどうかを選択してください。";
$l['error_section_missing_translation'] = "翻訳を使用するかどうかを選択してください。";
$l['error_missing_sid'] = "ドキュメントを入れる章を選択してください。";
$l['error_document_missing_name'] = "ドキュメントの名前を入力してください。";
$l['error_document_missing_description'] = "ドキュメントの説明を入力してください。";
$l['error_document_missing_document'] = "ドキュメントの本文を入力してください。";
$l['error_document_missing_enabled'] = "有効にするかどうかを選択してください。";
$l['error_document_missing_translation'] = "翻訳を使用するかどうかを選択してください。";
$l['error_invalid_sid'] = "指定されたドキュメントが間違っています。";
$l['error_missing_section_id'] = "指定された章が間違っています。";
$l['error_cannot_delete_section'] = "ヘルプのデフォルトの章は削除できません。";
$l['error_missing_hid'] = "指定されたドキュメントは存在しません。";
$l['error_cannot_delete_document'] = "ヘルプのデフォルトのドキュメントは削除できません。";
$l['success_help_section_added'] = "ヘルプの章を追加しました。";
$l['success_help_document_added'] = "ヘルプのドキュメントを追加しました。";
$l['success_help_section_updated'] = "ヘルプの章を更新しました。";
$l['success_help_document_updated'] = "ヘルプのドキュメントを更新しました。";
$l['success_section_deleted'] = "ヘルプの章を削除しました。";
$l['success_document_deleted'] = "ヘルプのドキュメントを削除しました。";
$l['confirm_section_deletion'] = "本当にこの章を削除しますか？";
$l['confirm_document_deletion'] = "本当にこのドキュメントを削除しますか？";
?>