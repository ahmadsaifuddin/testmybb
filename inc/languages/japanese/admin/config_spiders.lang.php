<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_spiders.lang.php 5833 2012-05-24 08:39:00Z Tomm $
 */
 
$l['spiders_bots'] = "クローラ/Bot";
$l['spiders_bots_desc'] = "ここでは検索エンジンによるクローラ&amp;botをフォーラムで自動検出するための管理ができます。また、クローラの最終訪問時刻を知ることもできます。";
$l['add_new_bot'] = "新しいクローラを追加";
$l['add_new_bot_desc'] = "ここではフォーラムが検出するクローラを追加できます。";
$l['edit_bot'] = "クローラを編集";
$l['edit_bot_desc'] = "ここでは登録済みのクローラを編集できます。";
$l['bot'] = "クローラを保存";
$l['last_visit'] = "最終訪問";
$l['no_bots'] = "現在、このフォーラムで追跡中の検索エンジンクローラはありません。";
$l['name'] = "名前";
$l['name_desc'] = "クローラを識別するための名前を入力してください。";
$l['user_agent'] = "ユーザエージェント文字列";
$l['user_agent_desc'] = "クローラのユーザエージェントにマッチする文字列を入力してください。（部分一致で認識可）";
$l['language_str'] = "言語";
$l['language_desc'] = "クローラに見せる言語パックを選択してください。";
$l['theme'] = "テーマ";
$l['theme_desc'] = "クローラに見せるテーマを選択してください。";
$l['user_group'] = "ユーザグループ";
$l['user_group_desc'] = "どのグループの権限を適用するのか選択してください。 (「ゲスト」以外に変更することはお勧めしません。)";
$l['save_bot'] = "クローラを保存";
$l['use_board_default'] = "掲示板のデフォルトを使用";
$l['error_invalid_bot'] = "指定されたクローラは存在しません。";
$l['error_missing_name'] = "クローラの名前が入力されていません。";
$l['error_missing_agent'] = "クローラのユーザエージェント文字列が入力されていません。";
$l['success_bot_created'] = "クローラ情報を作成しました。";
$l['success_bot_updated'] = "クローラ情報を更新しました。";
$l['success_bot_deleted'] = "指定されたクローラを削除しました。";
$l['confirm_bot_deletion'] = "本当にこのクローラ情報を削除しますか？";
?>