<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_badwords.lang.php 5783 2012-04-19 12:49:23Z Tomm $
 */

$l['bad_words'] = "NGワード";
$l['edit_bad_word'] = "NGワードを編集";
$l['edit_bad_word_desc'] = "ここではNGワードと、置き換えに使う文字列を定義できます。";
$l['bad_word_filters'] = "NGワード";
$l['bad_word_filters_desc'] = "この機能では、記事中に使われた不適切な語句を自動的に別の文字列に置き換える設定を管理します。罵詈雑言の類を置き換えるのに便利です。";
$l['bad_word'] = "NGワード";
$l['bad_word_desc'] = "NGワードを指定してください。'*' は任意の1文字を置き換えます。(a-zA-Z0-9_).";
$l['bad_word_max'] = "NBワードは100文字以内で指定してください。";
$l['replacement'] = "置換え文字列";
$l['replacement_desc'] = "NGワードに替わって表示する文字列を入力してください。(空欄の場合にはアスタリスクが表示されます)";
$l['replacement_word_max'] = "置換え文字列は100文字以内で指定してください。";
$l['error_replacement_word_invalid'] = "NGワードと同じものを置き換え文字列に指定することはできません。";
$l['save_bad_word'] = "NGワードを保存";
$l['no_bad_words'] = "現在、NGワードはありません。";
$l['add_bad_word'] = "NGワードを追加";
$l['error_missing_bad_word'] = "NGワードを入力してください。";
$l['error_invalid_bid'] = "指定されたNGワードが存在しません。";
$l['error_bad_word_filtered'] = "指定されたNGワードはすでに設定されています。";
$l['success_added_bad_word'] = "NGワードは正常に追加されました。";
$l['success_deleted_bad_word'] = "NGワードは正常に削除されました。";
$l['success_updated_bad_word'] = "NGワードは正常に更新されました。";
$l['confirm_bad_word_deletion'] = "本当にこのNGワードを削除しますか？";
?>