<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: home_module_meta.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */
 
$l['home'] = "ホーム";
$l['dashboard'] = "ダッシュボード";
$l['preferences'] = "環境設定";
$l['version_check'] = "バージョン確認";
$l['mybb_credits'] = "MyBBクレジット";
$l['add_new_forum'] = "新しいフォーラムを追加";
$l['search_for_users'] = "ユーザ検索";
$l['themes'] = "テーマ";
$l['templates'] = "テンプレート";
$l['plugins'] = "プラグイン";
$l['database_backups'] = "データベースバックアップ";
$l['quick_access'] = "クイックアクセス";
$l['online_admins'] = "オンライン管理者";
?>