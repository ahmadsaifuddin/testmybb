<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: home_credits.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['mybb_credits'] = "MyBBクレジット";
$l['mybb_credits_description'] = "MyBBを作成するにあたり、時間と労力を割いて貢献したのがこの方々です。";
$l['about_the_team'] = "チームについて";
$l['product_managers'] = "プロダクトマネージャ";
$l['developers'] = "開発者";
$l['graphics_and_style'] = "グラフィックス&amp;スタイル";
$l['software_quality_assurance'] = "ソフトウェア品質管理";
$l['support_representative'] = "サポート代表";
$l['pr_liaison'] = "PR活動窓口";
?>