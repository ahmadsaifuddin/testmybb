<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_banning.lang.php 5782 2012-04-19 11:56:13Z Tomm $
 */
 
$l['banning'] = "追放 / 禁止";
$l['banned_ips'] = "追放IP";
$l['banned_ips_desc'] = "ここでは掲示板へのアクセスを禁止するIPアドレスを管理します。";
$l['banned_accounts'] = "追放アカウント";
$l['disallowed_usernames'] = "禁止ユーザ名";
$l['disallowed_usernames_desc'] = "ここでは登録したり、ユーザが使用したりできないようにするユーザ名のリストを管理します。この機能は、ユーザ名の予約に便利です。";
$l['disallowed_email_addresses'] = "禁止メールアドレス";
$l['disallowed_email_addresses_desc'] = "ここでは登録したり、ユーザが使用したりできないようにするメールアドレスのリストを管理します。";
$l['banned_ip_addresses'] = "追放IP";
$l['username'] = "ユーザ名";
$l['date_disallowed'] = "禁止日";
$l['last_attempted_use'] = "使用されようとした最終日時";
$l['email_address'] = "メールアドレス";
$l['ip_address'] = "IPアドレス";
$l['ban_date'] = "追放日";
$l['last_access'] = "最終訪問";
$l['no_bans'] = "現在、追放の設定がありません。";
$l['add_disallowed_username'] = "禁止ユーザ名を追加";
$l['username_desc'] = "注意: * の文字はワイルドカードになります。";
$l['disallow_username'] = "ユーザ名を禁止";
$l['add_disallowed_email_address'] = "禁止メールアドレスを追加";
$l['email_address_desc'] = "注意: * の文字はワイルドカードになります。";
$l['disallow_email_address'] = "メールアドレスを禁止";
$l['ban_an_ip_address'] = "IPアドレスを追放";
$l['ip_address_desc'] = "注意: IPアドレスを範囲で追放するには * を使ってください。(例: 127.0.0.*)";
$l['ban_ip_address'] = "IPアドレスを追放";
$l['error_missing_ban_input'] = "追放するものが入力されていません。";
$l['error_invalid_filter'] = "指定されたフィルタが存在しません。";
$l['error_filter_already_banned'] = "指定されたフィルタはすでに追放されています。";
$l['success_ip_banned'] = "追放IPアドレスは正常に追加されました。";
$l['success_username_disallowed'] = "禁止ユーザ名は正常に追加されました。";
$l['success_email_disallowed'] = "禁止メールアドレスは正常に追加されました。";
$l['success_ban_deleted'] = "指定の禁止項目は正常に削除されました。";
$l['confirm_ban_deletion'] = "本当に削除しますか？";
?>
