<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_languages.lang.php 5828 2012-05-08 16:06:16Z Tomm $
 */

$l['languages'] = "言語パック";
$l['languages_desc'] = "インストールされているパッケージが下の一覧に表示されます。言語のファイルを編集したり、別の言語と比較しながら編集したり、言語のプロパティを編集したりできます。";
$l['find_language_packs'] = "言語パックを検索";
$l['language_files'] = "言語ファイル";
$l['language_files_desc'] = "下の一覧には言語パックに含まれているファイルが表示されています。フロントエンド用と管理者CP用に分類されているので、ファイルを選択して編集してください。";
$l['quick_phrases'] = "簡易編集";
$l['quick_phrases_desc'] = "一般的によく変更されるフレーズを手軽に編集できます。";
$l['edit_with'] = "{1}を参照しながら編集";
$l['edit_language_variables'] = "言語の変数を編集";
$l['edit_language_variables_desc'] = "言語のフレーズをお好みに応じて編集してください。";
$l['edit_properties_desc'] = "言語パックの環境設定を変更できます。";
$l['installed_language_packs'] = "インストールされている言語パック";
$l['front_end'] = "フロントエンド";
$l['admin_cp'] = "管理者CP";
$l['save_language_file'] = "言語ファイルを保存";
$l['nav_editing_set'] = "言語パックの環境設定";
$l['edit_properties'] = "言語パックの環境設定";
$l['friendly_name'] = "わかりやすい名前";
$l['language_in_html'] = "&lt;html&gt; タグに設定する言語";
$l['charset'] = "文字セット";
$l['admin'] = "管理者CPの言語変数が含まれていますか？";
$l['rtl'] = "右から左に記述する言語ですか？";
$l['quickphrases_agreement'] = "利用規約 - タイトル";
$l['quickphrases_agreement_1'] = "利用規約 - 段落1";
$l['quickphrases_agreement_2'] = "利用規約 - 段落2";
$l['quickphrases_agreement_3'] = "利用規約 - 段落3";
$l['quickphrases_agreement_4'] = "利用規約 - 段落4";
$l['quickphrases_agreement_5'] = "利用規約 - 段落5";
$l['quickphrases_error_nopermission_guest_1'] = "認証失敗メッセージ - 段落1";
$l['quickphrases_error_nopermission_guest_2'] = "認証失敗メッセージ - 段落2";
$l['quickphrases_error_nopermission_guest_3'] = "認証失敗メッセージ - 段落3";
$l['quickphrases_error_nopermission_guest_4'] = "認証失敗メッセージ - 段落4";
$l['no_languages'] = "言語がありません";
$l['no_language_files_front_end'] = "フロントエンド用の言語ファイルがありません。";
$l['no_language_files_admin_cp'] = "管理者CP用の言語ファイルがありません。";
$l['error_invalid_set'] = "指定された言語セットが間違っています。";
$l['error_invalid_file'] = "指定された言語ファイルが間違っています。";
$l['error_cannot_write_to_file'] = "ファイルに書き込めません。権限を確認の上、もう一度お試しください。";
$l['alert_note_cannot_write'] = "言語ファイルの編集の前に、サーバが言語ファイルに書き込みができるようCHMODで適切な権限を設定してください。";
$l['success_langfile_updated'] = "言語ファイルは正常に更新されました。";
$l['success_langprops_updated'] = "言語の環境設定は正常に更新されました。";
$l['success_quickphrases_updated'] = "言語の簡易編集は正常に更新されました。";
?>