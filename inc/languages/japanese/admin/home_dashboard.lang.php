<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: home_dashboard.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */
 
$l['dashboard'] = "ダッシュボード";
$l['dashboard_description'] = "ここでは掲示板に関するさまざまな統計データを閲覧できます。また、他の管理者と情報共有するためのメモも参照できます。";
$l['mybb_server_stats'] = "MyBB/サーバの統計";
$l['forum_stats'] = "フォーラム統計";
$l['mybb_version'] = "MyBBバージョン";
$l['threads'] = "スレッド";
$l['new_today'] = "本日作成";
$l['unapproved'] = "却下";
$l['php_version'] = "PHPバージョン";
$l['posts'] = "記事";
$l['sql_engine'] = "SQLエンジン";
$l['users'] = "ユーザ";
$l['registered_users'] = "登録ユーザ";
$l['active_users'] = "アクティブ";
$l['registrations_today'] = "本日登録";
$l['awaiting_activation'] = "有効化の手続き中";
$l['server_load'] = "サーバ負荷";
$l['attachments'] = "添付ファイル";
$l['used'] = "使用済み";
$l['last_update_check_two_weeks'] = "最後に<a href=\"{1}\">MyBBのバージョンをチェック</a>してから2週間以上経過しています。";
$l['new_version_available'] = "使用中のバージョンは{1}ですが、リリースされている最新バージョンは{2}です。";
$l['admin_notes_public'] = "このメモはすべての管理者に公開されます。";
$l['admin_notes'] = "管理者用メモ";
$l['save_notes'] = "メモを保存";
$l['success_notes_updated'] = "管理者用メモを更新しました。";
?>
