<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_mycode.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['custom_mycode'] = "カスタムMyCode";
$l['mycode'] = "MyCode";
$l['mycode_desc'] = "カスタムMyCodeで追加のMyCodeを作成し、メンバーがメッセージ作成時に使えるようにできます。";
$l['add_new_mycode'] = "新しいMyCodeを追加";
$l['add_new_mycode_desc'] = "ここでは新しいカスタムMycodeをフォーラムに追加できます。下にあるMyCodeサンドボックスを使えば、正規表現や置換の動作を保存前に確認することができます。";
$l['edit_mycode_desc'] = "ここではカスタムMyCodeの編集ができます。下にあるMyCodeサンドボックスを使えば、正規表現や置換の動作を保存前に確認することができます。";
$l['title'] = "タイトル";
$l['short_description'] = "簡単な説明";
$l['regular_expression'] = "正規表現";
$l['regular_expression_desc'] = "指定した組み合わせの文字列を検索する正規表現を入力してください。構文的に正しく安全な正規表現を入力してください。ここでは正規表現の検証は行いません。";
$l['replacement'] = "置換文字列";
$l['replacement_desc'] = "正規表現に対する置換文字列を入力してください。";
$l['example'] = "例:";
$l['enabled'] = "有効にしますか？";
$l['parse_order'] = "解析順";
$l['parse_order_desc'] = "MyCodeは他のMyCodeに対して昇順に解析されます。";
$l['edit_mycode'] = "MyCodeを編集";
$l['activate_mycode'] = "MyCodeを有効化";
$l['deactivate_mycode'] = "MyCodeを無効化";
$l['delete_mycode'] = "MyCodeを削除";
$l['no_mycode'] = "現在カスタムMyCodeはありません。";
$l['save_mycode'] = "MyCodeを保存";
$l['add_mycode'] = "MyCodeを追加";
$l['changes_not_saved'] = "変更はまだ保存されていません。";
$l['deactivated'] = "無効";
$l['sandbox'] = "サンドボックス";
$l['sandbox_desc'] = "変更を保存する前に、ここで正規表現と置換文字列の動作を確認することができます。";
$l['test_value'] = "テストする値";
$l['test_value_desc'] = "下のボックス内にテストのためのテキストを入力してください。";
$l['result_html'] = "HTMLの結果";
$l['result_html_desc'] = "下のテキストエリアにはMyCodeを適用して得られたHTMLが表示されます。";
$l['result_actual'] = "実際の結果";
$l['result_actual_desc'] = "下のエリアにはMyCode適用後のHTMLをブラウザを介して見た結果を表示します。";
$l['test'] = "MyCodeをテスト";
$l['error_missing_title'] = "タイトルが入力されていません。";
$l['error_missing_regex'] = "正規表現が入力されていません。";
$l['error_missing_replacement'] = "置換文字列が入力されていません。";
$l['error_invalid_mycode'] = "指定されたMyCodeが存在しません。";
$l['success_added_mycode'] = "MyCodeを追加しました。";
$l['success_deleted_mycode'] = "選択したMyCodeを削除しました。";
$l['success_updated_mycode'] = "MyCodeを更新しました。";
$l['success_deactivated_mycode'] = "選択したMyCodeを無効化しました。";
$l['success_activated_mycode'] = "選択したMyCodeを有効化しました。";
$l['confirm_mycode_deletion'] = "本当にこのMyCodeを削除しますか？";
?>