<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id$
 */
 
$l['today'] = "今日";
$l['yesterday'] = "昨日";
$l['size_yb'] = "YB";
$l['size_zb'] = "ZB";
$l['size_eb'] = "EB";
$l['size_pb'] = "PB";
$l['size_tb'] = "TB";
$l['size_gb'] = "GB";
$l['size_mb'] = "MB";
$l['size_kb'] = "KB";
$l['size_bytes'] = "バイト";
$l['na'] = "無効";
// Header language strings
$l['mybb_admin_panel'] = "MyBBコントロールパネル";
$l['mybb_admin_cp'] = "MyBB 管理者CP";
$l['logged_in_as'] = "ログイン中";
$l['view_board'] = "フォーラムを見る";
$l['logout'] = "ログアウト";
// Footer language strings
$l['generated_in'] = "生成時間{1}秒、{2}クエリ、メモリ消費{3}";
// Login page
$l['enter_username_and_password'] = "ユーザ名とパスワードを入力してください。";
$l['mybb_admin_login'] = "MyBB コントロールパネル - ログイン";
$l['return_to_forum'] = "フォーラムに戻る";
$l['please_login'] = "ログインしてください";
$l['username'] = "ユーザ名:";
$l['username1'] = "メール:";
$l['username2'] = "ユーザ名/メール:";
$l['password'] = "パスワード:";
$l['login'] = "ログイン";
$l['lost_password'] = "パスワードを忘れてしまいました";
$l['error_invalid_admin_session'] = "不正な管理セッション";
$l['error_admin_session_expired'] = "管理セッションの有効期限が切れました";
$l['error_invalid_ip'] = "このセッションに対してIPアドレスが不正です";
$l['error_mybb_admin_lockedout'] = "このアカウントは凍結されています";
$l['error_mybb_admin_lockedout_message'] = "ログインに{1}回失敗したため、アカウントを凍結しました。凍結解除のための手続き方法はメールで送信されています。";
$l['error_invalid_username'] = "入力されたユーザ名が間違っています。";
$l['error_invalid_uid'] = "入力されたユーザIDが間違っています。";
$l['error_invalid_token'] = "入力された有効化コードが間違っています。";
$l['success_logged_out'] = "ログアウトしました。";
$l['error_invalid_username_password'] = "ユーザ名とパスワードの組み合わせが間違っています。";
// Action Confirmation
$l['confirm_action'] = "本当にこのアクションを実行しますか？";
// Common words and phrases
$l['home'] = "ホーム";
$l['name'] = "名前";
$l['size'] = "サイズ";
$l['controls'] = "操作";
$l['view'] = "表示";
$l['yes'] = "はい";
$l['no'] = "いいえ";
$l['cancel'] = "キャンセル";
$l['options'] = "オプション";
$l['proceed'] = "続行";
$l['ok'] = "OK";
$l['error'] = "エラー";
$l['edit'] = "編集";
$l['never'] = "なし";
$l['legend'] = "凡例";
$l['version'] = "バージョン";
$l['languagevar'] = "言語";
$l['use_default'] = "デフォルトを使用";
$l['file'] = "ファイル";
$l['go'] = "実行";
$l['clear'] = "リセット";
$l['unknown'] = "不明";
$l['year'] = "年間";
$l['year_short'] = "年";
$l['years'] = "年間";
$l['years_short'] = "年";
$l['month'] = "か月";
$l['month_short'] = "月";
$l['months'] = "か月";
$l['months_short'] = "月";
$l['week'] = "週間";
$l['week_short'] = "週";
$l['weeks'] = "週間";
$l['weeks_short'] = "週";
$l['day'] = "日間";
$l['day_short'] = "日";
$l['days'] = "日間";
$l['days_short'] = "日";
$l['hour'] = "時間";
$l['hour_short'] = "時間";
$l['hours'] = "時間";
$l['hours_short'] = "時間";
$l['minute'] = "分間";
$l['minute_short'] = "分";
$l['minutes'] = "分間";
$l['minutes_short'] = "分";
$l['second'] = "秒間";
$l['second_short'] = "秒";
$l['seconds'] = "秒間";
$l['seconds_short'] = "秒";
$l['permanent'] = "永久";
$l['all_forums'] = "すべてのフォーラム";
$l['mybb_acp'] = "MyBB ACP";
$l['pages'] = "ページ";
$l['previous'] = "前へ";
$l['page'] = "ページ";
$l['next'] = "次へ";
$l['delete'] = "削除";
$l['reset'] = "リセット";
$l['and'] = "かつ";
$l['on'] = "On";
$l['off'] = "Off";
// Parser bits
$l['quote'] = "引用:";
$l['wrote'] = "曰く:";
$l['code'] = "コード:";
$l['php_code'] = "PHPコード:";
$l['linkback'] = "元の記事";
// The months of the year
$l['january'] = "1月";
$l['february'] = "2月";
$l['march'] = "3月";
$l['april'] = "4月";
$l['may'] = "5月";
$l['june'] = "6月";
$l['july'] = "7月";
$l['august'] = "8月";
$l['september'] = "9月";
$l['october'] = "10月";
$l['november'] = "11月";
$l['december'] = "12月";
// Access Denied
$l['access_denied'] = "アクセスが拒否されました";
$l['access_denied_desc'] = "管理者CPのこの場所へのアクセス権限がありません。";
// Super Administrator required
$l['cannot_perform_action_super_admin_general'] = "スーパー管理者でないため、このアクションは実行できません。<br /><br />このアクションを実行するためには、inc/config.phpのスーパー管理者のリストにお使いのユーザIDを追加してください。";
// AJAX
$l['loading_text'] = "ロード中<br />しばらくお待ちください";
// Time zone selection boxes
$l['timezone_gmt_minus_1200'] = "(GMT -12:00) エニウェトク、クェゼリン";
$l['timezone_gmt_minus_1100'] = "(GMT -11:00) ノーム、ミッドウェイ島、サモア";
$l['timezone_gmt_minus_1000'] = "(GMT -10:00) ハワイ";
$l['timezone_gmt_minus_900'] = "(GMT -9:00) アラスカ";
$l['timezone_gmt_minus_800'] = "(GMT -8:00) 太平洋標準時";
$l['timezone_gmt_minus_700'] = "(GMT -7:00) 山岳部標準時";
$l['timezone_gmt_minus_600'] = "(GMT -6:00) 中部標準時、メキシコシティ";
$l['timezone_gmt_minus_500'] = "(GMT -5:00) 東部標準時、ボゴタ、リマ、キト";
$l['timezone_gmt_minus_450'] = "(GMT -4:30) ベネズエラ (9月)";
$l['timezone_gmt_minus_400'] = "(GMT -4:00) 大西洋標準時、カラカス、ラ・パス";
$l['timezone_gmt_minus_350'] = "(GMT -3:30) ニューファンドランド";
$l['timezone_gmt_minus_300'] = "(GMT -3:00) ブラジル、ブエノスアイレス、ジョージタウン、フォークランド諸";
$l['timezone_gmt_minus_200'] = "(GMT -2:00) ミッド・アトランティック、アセンション島、セント・ヘレナ";
$l['timezone_gmt_minus_100'] = "(GMT -1:00) アゾレス諸島、カボベルデ諸島";
$l['timezone_gmt'] = "(GMT) カサブランカ、ダブリン、エディンバラ、ロンドン、リスボン、モンロビア";
$l['timezone_gmt_100'] = "(GMT +1:00) ベルリン、ブリュッセル、コペンハーゲン、マドリード、パリ、ローマ";
$l['timezone_gmt_200'] = "(GMT +2:00) カリーニングラード、南アフリカ、ワルシャワ";
$l['timezone_gmt_300'] = "(GMT +3:00) バグダッド、リヤド、モスクワ、ナイロビ";
$l['timezone_gmt_350'] = "(GMT +3:30) テヘラン";
$l['timezone_gmt_400'] = "(GMT +4:00) アブダビ、バクー、マスカット、トリビシ";
$l['timezone_gmt_450'] = "(GMT +4:30) カブール";
$l['timezone_gmt_500'] = "(GMT +5:00) イスラマバード、カラチ、タシケン";
$l['timezone_gmt_550'] = "(GMT +5:30) ムンバイ、カルカッタ、マドラス、ニューデリー";
$l['timezone_gmt_600'] = "(GMT +6:00) アルマトゥイ、コロンバ、ダッカ";
$l['timezone_gmt_700'] = "(GMT +7:00) バンコク、ハノイ、ジャカルタ";
$l['timezone_gmt_800'] = "(GMT +8:00) 北京、香港、パース、シンガポール、台北";
$l['timezone_gmt_900'] = "(GMT +9:00) 大阪、東京、札幌、ソウル、ヤクーツク";
$l['timezone_gmt_950'] = "(GMT +9:30) アデレード、ダーウィ";
$l['timezone_gmt_1000'] = "(GMT +10:00) メルボルン、パプアニューギニア、シドニー、ウラジオストーク";
$l['timezone_gmt_1100'] = "(GMT +11:00) マガダン、ニューカレドニア、ソロモン諸島";
$l['timezone_gmt_1200'] = "(GMT +12:00) オークランド、ウェリントン、フィジー、マーシャル諸島";
$l['timezone_gmt_short'] = "GMT {1}({2})";
// Global language strings used for log deletion pages
$l['confirm_delete_logs'] = "指定のログをプルーニングしますか？";
$l['confirm_delete_all_logs'] = "すべてのログをプルーニングしますか？";
$l['selected_logs_deleted'] = "選択されたログエントリを削除しました。";
$l['all_logs_deleted'] = "すべてのログエントリを削除しました。";
$l['delete_selected'] = "選択されたものを削除";
$l['delete_all'] = "フィルタされたものをすべて削除";
// Misc
$l['encountered_errors'] = "次のエラーが発生しました:";
$l['invalid_post_verify_key'] = "認証コードが一致しません。次に示すアクションを実行したいのか確認してください。";
$l['invalid_post_verify_key2'] = "認証コードが一致しません。正しいページをアクセスしているか確認してください。";
$l['forums_colon'] = "フォーラム:";
// Code buttons editor language strings
$l['editor_title_bold'] = "太字のテキスト";
$l['editor_title_italic'] = "斜体のテキスト";
$l['editor_title_underline'] = "下線つきテキスト";
$l['editor_title_left'] = "テキストを左寄せ";
$l['editor_title_center'] = "テキストを中央揃え";
$l['editor_title_right'] = "テキストを右寄せ";
$l['editor_title_justify'] = "テキストを均等割り付け";
$l['editor_title_numlist'] = "番号付きのリスト";
$l['editor_title_bulletlist'] = "黒丸のリスト";
$l['editor_title_image'] = "画像";
$l['editor_title_hyperlink'] = "リンク";
$l['editor_title_email'] = "メールアドレス";
$l['editor_title_quote'] = "引用";
$l['editor_title_code'] = "コード表示";
$l['editor_title_php'] = "整形済みのPHPコード";
$l['editor_title_close_tags'] = "閉じられていないMyCodeのタグをすべて閉じる";
$l['editor_enter_list_item'] = "リストの項目を入力してください。リストの項目をすべて入力したらキャンセルをクリックするか、最後の項目を空白にしてください。";
$l['editor_enter_url'] = "サイトのURL";
$l['editor_enter_url_title'] = "URLのタイトル（オプション）";
$l['editor_enter_email'] = "メールアドレス";
$l['editor_enter_email_title'] = "メールアドレスのタイトル（オプション）";
$l['editor_enter_image'] = "画像のURL";
$l['editor_enter_video_url'] = "動画のURL";
$l['editor_video_dailymotion'] = "Dailymotion";
$l['editor_video_metacafe'] = "MetaCafe";
$l['editor_video_myspacetv'] = "MySpace TV";
$l['editor_video_vimeo'] = "Vimeo";
$l['editor_video_yahoo'] = "Yahoo Video";
$l['editor_video_youtube'] = "YouTube";
$l['editor_size_xx_small'] = "超極小";
$l['editor_size_x_small'] = "極小";
$l['editor_size_small'] = "小";
$l['editor_size_medium'] = "標準";
$l['editor_size_large'] = "大";
$l['editor_size_x_large'] = "極大";
$l['editor_size_xx_large'] = "超極大";
$l['editor_font'] = "フォント";
$l['editor_size'] = "文字サイズ";
$l['editor_color'] = "フォントカラー";
$l['missing_task'] = "エラー: タスクファイルが存在しません。";
$l['task_backup_cannot_write_backup'] = "エラー: データベースバックアップタスクがディレクトリへの書き込みに失敗しました。";
$l['task_backup_ran'] = "データベースバックアップタスクが終了しました。";
$l['task_checktables_ran'] = "テーブルチェックタスクが終了し、壊れたテーブルは見つかりませんでした";
$l['task_checktables_ran_found'] = "注意: テーブルチェックタスクが終了し、{1}  個のテーブルを修復しました。";
$l['task_dailycleanup_ran'] = "デイリークリーンアップタスクが終了しました。";
$l['task_hourlycleanup_ran'] = "毎時クリーンアップタスクが終了しました。";
$l['task_logcleanup_ran'] = "ログクリーンアップタスクが終了し、古いログを削除しました。";
$l['task_promotions_ran'] = "プロモーションタスクが終了しました。";
$l['task_threadviews_ran'] = "スレッド表示タスクが終了しました。";
$l['task_usercleanup_ran'] = "ユーザクリーンアップタスクが終了しました。";
$l['task_massmail_ran'] = "一括メール配信タスクが終了しました";
$l['task_userpruning_ran'] = "ユーザプルーニングタスクが終了しました。";
$l['task_delayedmoderation_ran'] = "モデレートスケジュールタスクが終了しました。";
$l['task_massmail_ran_errors'] = "{1}への送信中に問題が発生しました: {2}";
$l['massmail_username'] = "ユーザ名";
$l['email_addr'] = "メールアドレス";
$l['board_name'] = "掲示板名称";
$l['board_url'] = "掲示板URL";
// Unlock ACP
$l['lockout_unlock'] = "管理者CPの凍結を解除";
$l['enter_username_and_token'] = "ユーザ名と有効化コードを入力して先に進んでください。";
$l['unlock_token'] = "有効化コード:";
$l['unlock_account'] = "アカウントの凍結を解除";
// Email message for if an admin account has been locked out
$l['locked_out_subject'] = "管理者アカウントは{1}に凍結されました。";
$l['locked_out_message'] = "{1},


{2}の管理者アカウントは{3}回ログインに失敗したため凍結されました。

凍結を解除するには、次のURLをブラウザで開いてください。

{4}/{5}/index.php?action=unlock&uid={7}&token={6}

上記リンクでうまくいかない場合には、次のリンクをお試しください。

{4}/{5}/index.php?action=unlock

ここで次の情報を入力します。
ユーザ名: {1}
有効化コード: {6}

-- 
{2}スタッフ";
$l['comma'] = "、";
// If the language string for "Username" is too cramped in the ACP Login box
// then use this to define how much larger you want the gap to be (in px)
// $l['login_field_width'] = "0";
?>
