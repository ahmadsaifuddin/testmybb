<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: forum_module_meta.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */
 
$l['forums_and_posts'] = "フォーラム &amp; 記事";
$l['forum_management'] = "フォーラム管理";
$l['forum_announcements'] = "フォーラム告知";
$l['moderation_queue'] = "モデレーションキュー";
$l['attachments'] = "添付ファイル";
$l['can_manage_forums'] = "フォーラムの管理";
$l['can_manage_forum_announcements'] = "フォーラム告知の管理";
$l['can_moderate'] = "記事、スレッド、添付ファイルのモデレート";
$l['can_manage_attachments'] = "添付ファイルの管理";
?>