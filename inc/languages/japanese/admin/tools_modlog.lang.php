<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: tools_modlog.lang.php 5828 2012-05-08 16:06:16Z Tomm $
 */


$l['mod_logs'] = "モデレータログ";
$l['mod_logs_desc'] = "ここではモデレータログを閲覧したり、プルーニングしたり、検索したりできます。このログには一般ユーザのアクション（自分の記事/スレッドの削除）とモデレータのすべてのアクションが記録されます。";
$l['prune_mod_logs'] = "モデレータログのプルーニング";
$l['prune_mod_logs_desc'] = "ここでは条件を指定してモデレータログをプルーニングできます。";
$l['no_modlogs'] = "指定された条件に該当するログエントリがありません。";
$l['username'] = "ユーザ名";
$l['date'] = "日付";
$l['action'] = "アクション";
$l['information'] = "情報";
$l['ipaddress'] = "IPアドレス";

$l['forum'] = "フォーラム:";
$l['thread'] = "スレッド:";
$l['post'] = "記事:";
$l['user_info'] = "ユーザ:";
$l['announcement'] = "告知:";

$l['filter_moderator_logs'] = "モデレータログを指定条件で絞り込み";
$l['forum_moderator'] = "フォーラムモデレータ:";
$l['sort_by'] = "並べ替え:";
$l['results_per_page'] = "ページ当たりの結果表示数:";
$l['all_moderators'] = "すべてのモデレータ";
$l['older_than'] = "過去&nbsp;";
$l['forum_name'] = "フォーラム名";
$l['thread_subject'] = "スレッドの件名";
$l['asc'] = "昇順";
$l['desc'] = "降順";
$l['in'] = "を";
$l['order'] = "に表示";
$l['days'] = "日より古いログを削除";
$l['prune_moderator_logs'] = "モデレータログをプルーニング";
$l['date_range'] = "期間指定:";
$l['all_forums'] = "すべてのフォーラム";
$l['success_pruned_mod_logs'] = "モデレータログをプルーニングしました。";
?>
