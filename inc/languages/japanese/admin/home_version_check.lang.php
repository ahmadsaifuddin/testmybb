<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: home_version_check.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['version_check'] = "バージョンの確認";
$l['version_check_description'] = "ここでは現在使用しているMyBBが最新かどうかの確認ができます。また、MyBBから最新のお知らせを直接見ることができます。";
$l['latest_mybb_announcements'] = "MyBBからの最新のお知らせ";
$l['dl_the_latest_mybb'] = "最新のMyBBをダウンロード";
$l['check_plugin_versions'] = "プラグインのバージョンを確認";
$l['your_version'] = "現在のバージョン";
$l['latest_version'] = "最新のバージョン";
$l['update_forum'] = "<a href=\"http://mybb.com\" target=\"_blank\">MyBB公式サイト</a>から最新バージョンを入手してアップグレードしてください。";
$l['read_more'] = "続きを読む";
$l['success_up_to_date'] = "現在実行中のMyBBは最新バージョンです。";
$l['error_out_of_date'] = "現在実行中のMyBBは最新ではありません。";
$l['error_communication'] = "バージョンサーバとの通信中に問題が発生しました。しばらく時間をおいてから再度お試しください。";
$l['error_fetch_news'] = "MyBBは公式サイトから最新のお知らせを取得できませんでした。";
?>