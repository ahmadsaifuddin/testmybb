<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: user_module_meta.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */
 
$l['users_and_groups'] = "ユーザ &amp; グループ";
$l['users'] = "ユーザ";
$l['groups'] = "グループ";
$l['user_titles'] = "タイトル";
$l['banning'] = "追放 / 禁止";
$l['admin_permissions'] = "管理者アクセス権";
$l['mass_mail'] = "一括メール送信";
$l['group_promotions'] = "グループ昇格";
$l['can_manage_users'] = "ユーザの管理を許可";
$l['can_manage_user_groups'] = "ユーザグループの管理";
$l['can_manage_user_titles'] = "ユーザタイトルの管理";
$l['can_manage_user_bans'] = "ユーザの追放の管理";
$l['can_manage_admin_permissions'] = "管理権限の管理";
$l['can_send_mass_mail'] = "一括メールの送信";
$l['can_manage_group_promotions'] = "グループ昇格の管理";
?>
