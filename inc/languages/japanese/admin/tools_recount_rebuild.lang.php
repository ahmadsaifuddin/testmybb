<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: tools_recount_rebuild.lang.php 5500 2011-07-19 08:58:09Z Tomm $
 */

$l['recount_rebuild'] = "再集計&amp;再構築";
$l['recount_rebuild_desc'] = "掲示板データの同期がとれていない場合に、データの再集計や再構築ができます。";
$l['data_per_page'] = "ページ当たりのデータ数";
$l['recount_stats'] = "統計を再集計";
$l['recount_stats_desc'] = "掲示板の統計を再集計し、掲示板トップページと統計ページに表示される統計データを更新します。";
$l['rebuild_forum_counters'] = "フォーラムカウンタを再構築";
$l['rebuild_forum_counters_desc'] = "実行すると、記事やスレッドの投稿数のカウント、およびフォーラムの最終投稿時刻を更新します。";
$l['rebuild_thread_counters'] = "スレッドカウンタを再構築";
$l['rebuild_thread_counters_desc'] = "実行すると、記事数/閲覧数とスレッドの最終投稿時刻を更新します。";
$l['recount_user_posts'] = "ユーザの記事数を再集計";
$l['recount_user_posts_desc'] = "実行すると、ユーザ別の投稿数をデータベース上の記事数を集計して更新します。投稿数カウントを無効に設定してあるフォーラムの記事は集計から除外します。";
$l['rebuild_attachment_thumbs'] = "添付ファイルサムネールの再構築";
$l['rebuild_attachment_thumbs_desc'] = "添付ファイルのサムネールを現在設定されているサイズに合わせて再構築します。またサムネールのない添付ファイルにサムネールを作成します。";
$l['success_rebuilt_forum_counters'] = "フォーラムのカウンタを再構築しました。";
$l['success_rebuilt_thread_counters'] = "スレッドのカウンタを再構築しました。";
$l['success_rebuilt_user_counters'] = "ユーザの記事数を再集計しました。";
$l['success_rebuilt_attachment_thumbnails'] = "添付ファイルのサムネールを再構築しました。";
$l['success_rebuilt_forum_stats'] = "フォーラムの統計を再構築しました。";
$l['confirm_proceed_rebuild'] = "「続行」をクリックすると再集計と再構築を続行します。";
$l['automatically_redirecting'] = "自動的に転送されます。";
?>