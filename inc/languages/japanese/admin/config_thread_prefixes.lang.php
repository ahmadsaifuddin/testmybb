<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: config_thread_prefixes.lang.php 5828 2012-05-08 16:06:16Z Tomm $
 */

$l['thread_prefixes'] = "スレッドプリフィックス";
$l['thread_prefixes_desc'] = "スレッドプリフィックスを使うと、ユーザがスレッドに割り当てるプリフィックスを定義できます。そしてフォーラム内でプリフィックスでフィルタできるようになります。";
$l['add_new_thread_prefix'] = "新しいスレッドプリフィックスを追加";
$l['add_new_thread_prefix_desc'] = "ここではスレッドプリフィックスを作成し、どこで、またどのグループに適用するのか定義できます。";
$l['edit_prefix'] = "プリフィックスを編集";
$l['edit_prefix_desc'] = "ここではスレッドプリフィックスを編集して、どのように表示するか、どこで、またどのグループに適用するのかを変更できます。";
$l['edit_thread_prefix'] = "スレッドプリフィックスを編集";
$l['delete_thread_prefix'] = "スレッドプリフィックスを削除";
$l['prefix_options'] = "プリフィックスオプション";
$l['save_thread_prefix'] = "スレッドプリフィックスを保存";
$l['prefix'] = "プリフィックス";
$l['prefix_desc'] = "選択メニューに表示するテキスト版のプリフィックス";
$l['display_style'] = "表示スタイル";
$l['display_style_desc'] = "スレッド件名の次に実際に表示するプリフィックス。HTMLも使えますが、シンプルにテキスト版と同じものでもかまいません。";
$l['available_in_forums'] = "適用するフォーラム";
$l['available_to_groups'] = "適用するグループ";
$l['all_forums'] = "すべてのフォーラム";
$l['all_groups'] = "すべてのグループ";
$l['select_forums'] = "フォーラムを選択";
$l['select_groups'] = "グループを選択";
$l['groups_colon'] = "グループ:";
$l['no_thread_prefixes'] = "定義済みのスレッドプリフィックスはありません。";
$l['confirm_thread_prefix_deletion'] = "本当にこのスレッドプリフィックスを削除しますか？注意: 削除した後、このプリフィックスを使っているカスタムモデレータツールを忘れずに更新してください。";
$l['success_thread_prefix_created'] = "スレッドプリフィックスを作成しました。";
$l['success_thread_prefix_updated'] = "スレッドプリフィックスを更新しました。";
$l['success_thread_prefix_deleted'] = "スレッドプリフィックスを削除しました。このプリフィックスを使用しているカスタムモデレータツールがあれば更新してください。";
$l['error_missing_prefix'] = "追加したいプリフィックスを入力してください。";
$l['error_missing_display_style'] = "このプリフィックスの表示スタイルを入力してください。";
$l['error_no_forums_selected'] = "このプリフィックスを適用するフォーラムを選択してください。";
$l['error_no_groups_selected'] = "このプリフィックスを適用するグループを選択してください。";
$l['error_invalid_prefix'] = "指定されたスレッドプリフィックスは存在しません。";
?>