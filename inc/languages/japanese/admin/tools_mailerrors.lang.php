<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: tools_mailerrors.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['system_email_log'] = "システムメールログ";
$l['system_email_log_desc'] = "MyBBが生成したメールが送信時にエラーとなると、ログとして記録され下に一覧表示されます。このセクションはSMTPの設定やサーバのメールサポート状態を調べるのに役立ちます。";
$l['prune_system_email_log'] = "システムメールログをプルーニング";
$l['filter_system_email_log'] = "システムメールログを指定条件で絞り込み";
$l['close_window'] = "ウィンドウを閉じる";
$l['user_email_log_viewer'] = "ユーザメールビューア";
$l['smtp_code'] = "SMTPコード:";
$l['smtp_server_response'] = "SMTPサーバの応答:";
$l['to'] = "宛先";
$l['from'] = "差出人";
$l['subject'] = "件名";
$l['date'] = "日時";
$l['email'] = "メールアドレス";
$l['date_sent'] = "送信日";
$l['error_message'] = "エラーメッセージ";
$l['fine'] = "検索";
$l['no_logs'] = "検索条件に該当するログエントリが見つかりませんでした。";
$l['subject_contains'] = "件名に次の文字列を含む";
$l['error_message_contains'] = "エラーメッセージに次の文字列を含む";
$l['to_address_contains'] = "宛先アドレスに次の文字列を含む";
$l['from_address_contains'] = "送信元アドレスに次の文字列を含む";
$l['find_emails_to_addr'] = "次のアドレス宛てに送信されたメールをすべて検索";
?>