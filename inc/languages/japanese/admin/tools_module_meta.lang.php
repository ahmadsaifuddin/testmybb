<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: tools_module_meta.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['tools_and_maintenance'] = "ツール &amp; メンテナンス";
$l['maintenance'] = "メンテナンス";
$l['logs'] = "ログ";
$l['system_health'] = "システム状態";
$l['cache_manager'] = "キャッシュマネージャ";
$l['task_manager'] = "タスクマネージャ";
$l['recount_and_rebuild'] = "再集計 &amp; 再構築";
$l['view_php_info'] = "PHP情報の表示";
$l['database_backups'] = "データベースバックアップ";
$l['optimize_database'] = "データベース最適化";
$l['file_verification'] = "ファイルの検証";
$l['administrator_log'] = "管理者ログ";
$l['moderator_log'] = "モデレータログ";
$l['user_email_log'] = "ユーザメールログ";
$l['system_mail_log'] = "システムメールログ";
$l['user_warning_log'] = "ユーザ警告ログ";
$l['statistics'] = "統計情報";
$l['can_access_system_health'] = "システム状態へのアクセス";
$l['can_manage_cache'] = "キャッシュの管理";
$l['can_manage_tasks'] = "スケジュールされたタスクの管理";
$l['can_manage_db_backup'] = "データベースのバックアップの管理";
$l['can_optimize_db'] = "データベースの最適化";
$l['can_recount_and_rebuild'] = "再集計と再構築";
$l['can_manage_admin_logs'] = "管理者ログの管理";
$l['can_manage_mod_logs'] = "モデレータログの管理";
$l['can_manage_user_mail_log'] = "ユーザメールログの管理";
$l['can_manage_system_mail_log'] = "システムメールログの管理";
$l['can_manage_user_warning_log'] = "ユーザ警告ログの管理";
$l['can_view_php_info'] = "PHP情報の閲覧";
$l['can_manage_file_verification'] = "ファイルの検証";
$l['can_view_statistics'] = "統計情報の閲覧";
?>