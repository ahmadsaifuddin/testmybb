<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: tools_maillogs.lang.php 5828 2012-05-08 16:06:16Z Tomm $
 */

$l['user_email_log'] = "ユーザメールログ";
$l['user_email_log_desc'] = "メンバー間で送信されるメッセージや「スレッドを友人に送信」機能を使って送られたメールはすべてログとして記録され、下に表示されます。メールを悪用している可能性のある者をここで特定することができます。";
$l['prune_user_email_log'] = "ユーザメールログをプルーニング";
$l['close_window'] = "ウィンドウを閉じる";
$l['user_email_log_viewer'] = "ユーザメールログビューア";
$l['to'] = "宛先";
$l['from'] = "送信元";
$l['ip_address'] = "IPアドレス";
$l['subject'] = "件名";
$l['date'] = "日付";
$l['email'] = "メール";
$l['date_sent'] = "送信日";
$l['deleted'] = "削除済み";
$l['sent_using_send_thread_feature'] = "「友人にスレッドを送信」機能を利用して送信";
$l['thread'] = "スレッド:";
$l['find_emails_by_user'] = "このユーザが送信したメールを検索";
$l['find'] = "検索";
$l['deleted_user'] = "削除済みのユーザ";
$l['email_sent_to_user'] = "次のユーザ宛てのメール";
$l['no_logs'] = "指定された検索条件に該当するログエントリが見つかりませんでした。";
$l['filter_user_email_log'] = "ユーザメールログを指定条件で絞り込み";
$l['username_is'] = "ユーザ名が";
$l['email_contains'] = "メールアドレスに次の文字列を含む";
$l['subject_contains'] = "件名に次の文字列を含む";
$l['find_emails_to_user'] = "このユーザ宛てに送信されたすべてのメールを検索";
$l['error_invalid_user'] = "入力されたユーザは存在しません。";
?>