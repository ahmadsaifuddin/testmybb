<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: tools_optimizedb.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['optimize_database'] = "データベース最適化";
$l['table_selection'] = "テーブル選択";
$l['tables_select_desc'] = "ここでは操作するデータベーステーブルを選択できます。複数選択する場合には、CTRLキーを使用してください。";
$l['select_all'] = "すべて選択";
$l['deselect_all'] = "すべて選択解除";
$l['select_forum_tables'] = "フォーラムテーブルを選択";
$l['optimize_selected_tables'] = "選択したテーブルを最適化";
$l['error_no_tables_selected'] = "最適化するデータベーステーブルが選択されていません。";
$l['success_tables_optimized'] = "選択されたテーブルを解析、最適化しました。";
?>