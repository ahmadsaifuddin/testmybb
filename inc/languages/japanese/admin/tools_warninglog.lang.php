<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: tools_warninglog.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */


$l['warning_logs'] = "警告ログ";
$l['warning_logs_desc'] = "ここではユーザに対する警告の履歴が見られます。";
$l['warned_user'] = "警告されたユーザ";
$l['warning'] = "警告";
$l['date_issued'] = "警告日";
$l['expires'] = "期限";
$l['expiry_date'] = "期限";
$l['issued_date'] = "警告日";
$l['issued_by'] = "警告者";
$l['details'] = "詳細";
$l['filter_warning_logs'] = "警告ログを指定条件で絞り込み";
$l['filter_warned_user'] = "警告されたユーザ";
$l['filter_issued_by'] = "警告したユーザ:";
$l['filter_reason'] = "警告の理由（部分一致）:";
$l['sort_by'] = "表示順:";
$l['results_per_page'] = "ページ当たりの表示数:";
$l['view'] = "表示";
$l['no_warning_logs'] = "警告ログがありません。";
$l['revoked'] = "取り消し";
$l['post'] = "記事";
$l['asc'] = "昇順";
$l['desc'] = "降順";
$l['in'] = "順に並べ替えて";
$l['order'] = "に表示する";
$l['warning_details'] = "警告内容";
$l['warning_note'] = "管理者用メモ";
$l['already_expired'] = "期限切れ";
$l['warning_revoked'] = "取消し済み";
$l['warning_active'] = "有効";
$l['error_invalid_warning'] = "指定された警告が間違っています。";
$l['revoke_warning'] = "この警告を取り消す";
$l['revoke_warning_desc'] = "警告を取り消すために、理由を入力してください。警告を取り消しても、警告による追放や保留されたものがもとに戻されるわけではありません。";
$l['reason'] = "理由:";
$l['warning_is_revoked'] = "警告を取り消しました。";
$l['revoked_by'] = "取り消したユーザ:";
$l['date_revoked'] = "取り消した日:";
$l['error_already_revoked'] = "この警告はすでに取り消し済みです。";
$l['error_no_revoke_reason'] = "この警告を取り消す理由が入力されていません。";
$l['redirect_warning_revoked'] = "警告を取り消し、ユーザの警告ポイントを減らしました。";
?>
