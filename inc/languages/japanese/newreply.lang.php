<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: newreply.lang.php 5657 2011-11-27 12:32:47Z Tomm $
 */

$l['nav_newreply'] = "返信を投稿";
$l['post_reply_to'] = "{1}への返信";
$l['post_new_reply'] = "新しい返信を投稿";
$l['reply_to'] = "スレッド{1}への返信";
$l['post_subject'] = "返信の件名:";
$l['your_message'] = "本文:";
$l['post_options'] = "投稿オプション:";
$l['options_sig'] = "<strong>署名:</strong> 署名を入れる。(登録ユーザのみ)";
$l['options_emailnotify'] = "<strong>メール通知:</strong> 新たに返信があればメールで通知 (登録ユーザのみ)";
$l['options_disablesmilies'] = "<strong>スマイリーを無効:</strong> この記事ではスマイリーを表示ない";
$l['post_reply'] = "送信";
$l['preview_post'] = "プレビュー";
$l['mod_options'] = "モデレータオプション:";
$l['close_thread'] = "<strong>スレッドを締め切る</strong>: これ以上返信をつけられないようにする";
$l['stick_thread'] = "<strong>スレッドを固定:</strong> このスレッドをフォーラムの先頭に固定する";
$l['forum_rules'] = "{1} - ルール";
$l['thread_review'] = "スレッドレビュー (新規順)";
$l['thread_review_more'] = "{1}以上の返信があります。<a href=\"{2}\">スレッド全体を読む</a>";
$l['posted_by'] = "投稿者";
$l['draft_saved'] = "新しい記事は下書き保存されました。<br />下書きの一覧に移動します。";
$l['image_verification'] = "画像認証";
$l['verification_note'] = "画像に表示されているテキストをテキストボックスに入力してください。自動投稿を防止するための処理です。";
$l['verification_subnote'] = "(大文字/小文字を区別)";
$l['invalid_captcha'] = "画像認証コードが間違っています。画像に表示されているとおりに入力してください。";
$l['error_post_already_submitted'] = "すでにスレッドに返信として投稿済みです。スレッドに移動して投稿した返信をご覧ください。";
$l['multiquote_external_one'] = "別のスレッドの記事が1個選択されています。";
$l['multiquote_external'] = "別のスレッドの記事が{1}個選択されています。";
$l['multiquote_external_one_deselect'] = "選択を解除";
$l['multiquote_external_deselect'] = "選択を解除";
$l['multiquote_external_one_quote'] = "この記事も引用";
$l['multiquote_external_quote'] = "この記事も引用";
$l['redirect_newreply'] = "記事は投稿されました。";
$l['redirect_newreply_moderation'] = "管理者の設定により、すべての記事はモデレートが必要です。スレッドに戻ります。";
$l['redirect_newreply_post'] = "<br />記事に移動します。";
$l['redirect_newreplyerror'] = "内容不足により返信が却下されました。<br />スレッドに戻ります。";
$l['redirect_threadclosed'] = "モデレータがスレッドを締め切ったため、返信できません。";
$l['error_post_noperms'] = "この下書きを編集する権限がありません。";
?>
