<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: showthread.lang.php 5828 2012-05-08 16:06:16Z Tomm $
 */

$l['delete_poll'] = "アンケートを削除";
$l['close_thread'] = "スレッドを締め切る";
$l['stick_thread'] = "スレッドを固定";
$l['author'] = "投稿者";
$l['message'] = "メッセージ";
$l['threaded'] = "スレッドモード";
$l['linear'] = "全表示モード";
$l['next_oldest'] = "古いスレッドへ";
$l['next_newest'] = "新しいスレッドへ";
$l['view_printable'] = "印刷用ページを表示";
$l['send_thread'] = "このスレッドを友人に送信";
$l['subscribe_thread'] = "このスレッドをウォッチ";
$l['unsubscribe_thread'] = "このスレッドのウォッチを解除";
$l['moderation_options'] = "モデレートオプション:";
$l['delayed_moderation'] = "モデレートをスケジュール";
$l['thread_notes'] = "スレッドメモの編集/表示";
$l['open_close_thread'] = "スレッドを締め切る/返信受付中にする";
$l['approve_thread'] = "スレッドを承認";
$l['unapprove_thread'] = "スレッドを却下";
$l['delete_thread'] = "スレッドを削除";
$l['delete_posts'] = "選択した記事を削除";
$l['move_thread'] = "スレッドを移動/コピー";
$l['stick_unstick_thread'] = "スレッドを固定/固定解除";
$l['split_thread'] = "スレッドを分割";
$l['merge_threads'] = "スレッドをマージ";
$l['merge_posts'] = "選択した記事をマージ";
$l['remove_redirects'] = "転送を解除";
$l['remove_subscriptions'] = "すべてのウォッチを解除";
$l['poll'] = "アンケート:";
$l['show_results'] = "結果を表示";
$l['edit_poll'] = "アンケートを編集";
$l['public_note'] = "<b>注意:</b> このアンケートは公開されているため、他のユーザが回答内容が見ることができます。";
$l['total'] = "合計";
$l['vote'] = "回答";
$l['total_votes'] = "回答数{1}";
$l['you_voted'] = "* これを選択しました";
$l['poll_closed'] = "アンケートは締め切られています。";
$l['already_voted'] = "すでに回答済みです。";
$l['undo_vote'] = "回答の取り消し";
$l['quick_reply'] = "クイック返信";
$l['message_note'] = "このメッセージへの返信を入力してください。";
$l['signature'] = "署名";
$l['email_notify'] = "メール通知";
$l['disable_smilies'] = "スマイリーを無効";
$l['post_reply'] = "返信を投稿";
$l['post_reply_img'] = "返信を投稿";
$l['post_thread'] = "スレッドを投稿";
$l['preview_post'] = "記事をプレビュー";
$l['rating_average'] = "回答数{1} - 平均{2}";
$l['rate_thread'] = "このスレッドを評価:";
$l['thread_rating'] = "スレッドの評価:";
$l['similar_threads'] = "関連しそうなスレッド...";
$l['thread'] = "スレッド:";
$l['replies'] = "返信数:";
$l['views'] = "閲覧数:";
$l['lastpost'] = "最終投稿";
$l['messages_in_thread'] = "スレッド内のメッセージ数";
$l['users_browsing_thread'] = "このスレッドを閲覧中のユーザ:";
$l['users_browsing_thread_guests'] = "ゲスト{1}人";
$l['users_browsing_thread_invis'] = "非表示ユーザ{1}人";
$l['users_browsing_thread_reading'] = "読み込み中...";
$l['inline_delete_posts'] = "記事を削除";
$l['inline_merge_posts'] = "記事をマージ";
$l['inline_split_posts'] = "記事を分割";
$l['inline_approve_posts'] = "記事を承認";
$l['inline_unapprove_posts'] = "記事を却下";
$l['inline_post_moderation'] = "モデレータ用クイックツール:";
$l['inline_go'] = "実行";
$l['clear'] = "リセット";
$l['thread_closed'] = "締め切られたスレッド";
$l['no_subject'] = "無題";
$l['error_nonextnewest'] = "直前に表示していたスレッドより新しいスレッドはありません。";
$l['error_nonextoldest'] = "直前に表示していたスレッドより古いスレッドはありません。";
$l['quickreply_multiquote_selected'] = "引用する記事を選択してください。";
$l['quickreply_multiquote_now'] = "記事を引用";
$l['or'] = "または";
$l['quickreply_multiquote_deselect'] = "選択解除";
$l['search_thread'] = "スレッドを検索";
$l['enter_keywords'] = "キーワードを入力";
$l['image_verification'] = "画像認証";
$l['verification_note'] = "画像に表示されているテキストをテキストボックスに入力してください。自動投稿を防止するための処理です。";
$l['verification_subnote'] = "(大文字小文字を区別)";
?>
