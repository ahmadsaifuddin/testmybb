<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: sendthread.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['nav_sendthread'] = "友人にスレッドを送信";
$l['send_thread'] = "友人に送信";
$l['recipient'] = "宛先:";
$l['recipient_note'] = "友人のメールアドレスを入力してください。";
$l['subject'] = "件名:";
$l['message'] = "本文:";
$l['error_nosubject'] = "スレッドを送信するメールの件名を入力してください。（必須）";
$l['error_nomessage'] = "スレッドを送信するメールの本文を入力してください。（必須）";
?>