<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: warnings.lang.php 5814 2012-04-20 14:36:07Z Tomm $
 */
 
$l['nav_profile'] = "{1}のプロフィール";
$l['nav_warning_log'] = "警告ログ";
$l['nav_add_warning'] = "ユーザに警告する";
$l['nav_view_warning'] = "警告情報";
$l['warning_for_post'] = ".. 記事t:";
$l['already_expired'] = "期限切れ";
$l['warning_active'] = "有効";
$l['warning_revoked'] = "取り消し済み";
$l['warning_log'] = "警告ログ";
$l['warning'] = "警告";
$l['issued_by'] = "発行者";
$l['date_issued'] = "発行日";
$l['expiry_date'] = "期限";
$l['active_warnings'] = "有効な警告";
$l['expired_warnings'] = "期限切れの警告";
$l['warning_points'] = "({1}ポイント)";
$l['no_warnings'] = "このユーザは警告を受け取っていません。またはすべて削除されています。";
$l['warn_user'] = "ユーザーに警告";
$l['post'] = "記事:";
$l['warning_note'] = "管理用メモ:";
$l['warning_type'] = "警告タイプ:";
$l['custom'] = "理由を記入";
$l['reason'] = "理由:";
$l['points'] = "ポイント:";
$l['warn_user_desc'] = "このユーザが破った規則の数に応じて警告レベルを上げていくことができます。";
$l['send_pm'] = "ユーザへの通知:";
$l['send_user_warning_pm'] = "このユーザに警告を知らせるプライベートメッセージを送る。";
$l['send_pm_subject'] = "件名:";
$l['warning_pm_subject'] = "警告を受けました。";
$l['send_pm_message'] = "本文:";
$l['warning_pm_message'] = "{1}殿

{2}スタッフより警告を受けました。

-- ";
$l['expiration_never'] = "永久";
$l['expiration_hours'] = "時間";
$l['expiration_days'] = "日間";
$l['expiration_weeks'] = "週間";
$l['expiration_months'] = "か月";
$l['redirect_warned_banned'] = "ユーザは{2} {1}グループに移動されました。";
$l['redirect_warned_suspended'] = "このユーザが投稿する権限は{1}保留されます。";
$l['redirect_warned_moderate'] = "このユーザの投稿はすべて{1}モデレートが必要になります。";
$l['redirect_warned'] = "{1}の警告レベルは{2}%に上昇しました。{3}<br /></br>もとの場所に戻ります。";
$l['error_warning_system_disabled'] = "管理者が無効にしているため警告システムは利用できません。";
$l['reached_max_warnings_day'] = "警告数の既定の上限に達したため、今日はこれ以上警告することができません。<br /><br />1日に警告できるのは最大{1}回までです。";
$l['user_reached_max_warning'] = "このユーザはすでに警告レベルが最大に達しているため、これ以上警告することができません。";
$l['error_cant_warn_group'] = "このグループに所属するユーザに警告する権限がありません。";
$l['error_no_note'] = "警告の管理用メモが記入されていません。";
$l['error_invalid_type'] = "間違った警告タイプが選択されました。";
$l['error_invalid_user'] = "選択したユーザは存在しません。";
$l['error_invalid_post'] = "選択した記事は存在しません。";
$l['error_cant_custom_warn'] = "ユーザに警告する権限がありません。";
$l['error_no_custom_reason'] = "カスタム警告に理由が記入されていません。";
$l['error_invalid_custom_points'] = "ユーザの警告レベルに追加するポイントの数字が間違っています。0以上{1}以下の数字を入力してください。";
$l['details'] = "詳細";
$l['view'] = "表示";
$l['current_warning_level'] = "現在の警告レベル: <strong>{1}%</strong> ({2}/{3})";
$l['warning_details'] = "警告の詳細";
$l['revoke_warning'] = "この警告を取り消す";
$l['revoke_warning_desc'] = "警告を取り消すには、理由を入力してください。警告を取り消しても、警告による追放や保留されたものがもとに戻されるわけではありません。";
$l['warning_is_revoked'] = "この警告を取り消しました。";
$l['revoked_by'] = "取り消した人:";
$l['date_revoked'] = "取り消した日:";
$l['warning_already_revoked'] = "この警告はすでに取り消されています。";
$l['no_revoke_reason'] = "この警告を取り消す理由が入力されていません。";
$l['redirect_warning_revoked'] = "警告を取り消し、ユーザの警告ポイントを減少させました。<br /><br />警告ページに移動します。";
$l['result'] = "結果:";
$l['result_banned'] = "ユーザを追放グループ ({2}) に{2}移動します。";
$l['result_suspended'] = "投稿権限は{1}保留されます。";
$l['result_moderated'] = "投稿は{1}モデレートが必要となります。";
$l['result_period'] = "{1} {2}";
$l['hour_or_hours'] = "時間";
$l['day_or_days'] = "日間";
$l['week_or_weeks'] = "週間";
$l['month_or_months'] = "か月";
$l['expires'] = "期限:";
$l['new_warning_level'] = "新しい警告レベル:";
$l['cannot_warn_self'] = "自分自身の警告レベルを上昇させることはできません。";
$l['error_cant_warn_user'] = "このユーザに警告する権限がありません。";
$l['existing_post_warnings'] = "この記事への警告";
?>