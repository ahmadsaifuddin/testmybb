<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 *
 * $Id: datahandler_event.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['eventdata_missing_name'] = "イベント名がありません。イベント名を入力してください。";
$l['eventdata_missing_description'] = "イベントの説明がありません。イベントに関する説明を入力してください。";
$l['eventdata_invalid_start_date'] = "指定されたイベント開始日が間違っています。年、月、日の組み合わせが正しい日付となっているか、ご確認ください。";
$l['eventdata_invalid_start_year'] = "イベントは現在から5年以内のものしか作れません。リストから適切な年を選択してください。";
$l['eventdata_invalid_start_month'] = "開始年が間違っています。正しい開始年を指定してください。";
$l['eventdata_invalid_end_date'] = "イベントの終了日が間違っています。年、月、日の組み合わせが正しい日付となっているか、ご確認ください。";
$l['eventdata_invalid_end_year'] = "イベントは現在から5年以内のものしか作れません。リストから適切な年を選択してください。";
$l['eventdata_invalid_end_month'] = "終了月が間違っています。正しい終了月を入力してください。";
$l['eventdata_invalid_end_day'] = "終了日が間違っています。指定した月の日数より大きい数字を指定していないか、ご確認ください。";
$l['eventdata_cant_specify_one_time'] = "イベント開始時刻を指定するときには、終了時刻も指定する必要があります。";
$l['eventdata_start_time_invalid'] = "開始時刻の指定が間違っています。12am、12:01am、00:01のような形式で指定してください。";
$l['eventdata_end_time_invalid'] = "終了時刻の指定が間違っています。12am、12:01am、00:01のような形式で指定してください。";
$l['eventdata_invalid_timezone'] = "選択したタイムゾーンが間違っています。";
$l['eventdata_end_in_past'] = "開始日（時刻）より前の終了日（時刻）が指定されています。";
$l['eventdata_only_ranged_events_repeat'] = "開始と終了で期間指定してあるイベントのみ繰り返しが可能です。";
$l['eventdata_invalid_repeat_day_interval'] = "繰り返し間隔の日数が間違っています。";
$l['eventdata_invalid_repeat_week_interval'] = "繰り返し間隔の週数が間違っています。";
$l['eventdata_invalid_repeat_weekly_days'] = "イベントの曜日が指定されていません。";
$l['eventdata_invalid_repeat_month_interval'] = "繰り返し間隔の月数が間違っています。";
$l['eventdata_invalid_repeat_year_interval'] = "繰り返し間隔の年数が間違っています。";
$l['eventdata_event_wont_occur'] = "開始時刻と終了時刻を指定したときに繰り返しを設定すると、イベントが起きません。";
$l['eventdata_no_permission_private_event'] = "プライベートイベントを作成する権限がありません。";
?>