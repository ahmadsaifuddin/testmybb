<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: usercpnav.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['ucp_nav_width'] = "180";
$l['ucp_nav_menu'] = "メニュー";
$l['ucp_nav_messenger'] = "メッセンジャー";
$l['ucp_nav_compose'] = "新規作成";
$l['ucp_nav_tracking'] = "メッセージを追跡調査";
$l['ucp_nav_edit_folders'] = "フォルダを編集";
$l['ucp_nav_profile'] = "プロフィール";
$l['ucp_nav_edit_profile'] = "プロフィールを編集";
$l['ucp_nav_edit_options'] = "オプションを編集";
$l['ucp_nav_change_email'] = "メールアドレス変更";
$l['ucp_nav_change_pass'] = "パスワード変更";
$l['ucp_nav_change_username'] = "ユーザ名変更";
$l['ucp_nav_edit_sig'] = "署名を編集";
$l['ucp_nav_change_avatar'] = "アバター変更";
$l['ucp_nav_misc'] = "その他";
$l['ucp_nav_editlists'] = "友人 / 無視リスト";
$l['ucp_nav_favorite_threads'] = "お気に入りスレッド";
$l['ucp_nav_subscribed_threads'] = "ウォッチ中のスレッド";
$l['ucp_nav_forum_subscriptions'] = "ウォッチ中のフォーラム";
$l['ucp_nav_drafts'] = "保存された下書き";
$l['ucp_nav_notepad'] = "個人用メモ";
$l['ucp_nav_view_profile'] = "プロフィールを見る";
$l['ucp_nav_home'] = "ユーザCPホーム";
$l['ucp_nav_usergroups'] = "所属グループ";
$l['ucp_nav_attachments'] = "添付ファイルを管理";
?>
