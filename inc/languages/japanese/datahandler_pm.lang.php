<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 *
 * $Id: datahandler_pm.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['pmdata_too_long_subject'] = "プライベートメッセージの件名が長すぎます。短くしてください。";
$l['pmdata_code_no_subject'] = "[件名なし]";
$l['pmdata_missing_subject'] = "件名が入力されていません。件名を入力してください。";
$l['pmdata_missing_message'] = "本文が入力されていません。本文を入力してください。";
$l['pmdata_invalid_recipients'] = "指定された宛先の中に、登録されていないユーザ名（{1}）があります。";
$l['pmdata_no_recipients'] = "宛先が指定されていません。少なくともひとりのユーザ名を宛先に入力してください。";
$l['pmdata_too_many_recipients'] = "同時に送信できるのは最大 {1} 名までです。";
$l['pmdata_recipient_is_ignoring'] = "無視リストに入れられているため、{1} に送信ができません。";
$l['pmdata_recipient_has_buddy_only'] = "友人リストに入っていないため {1} に送信できません。";
$l['pmdata_recipient_pms_disabled'] = "{1} はプライベートメッセージを無効にしているため、メッセージを送信できません。";
$l['pmdata_recipient_reached_quota'] = "{1} はプライベートメッセージの容量が上限に達しているため、メッセージを送信できません。";
$l['pmdata_pm_flooding'] = "メッセージの送信間隔が短すぎます。前回の送信から {1} 秒以上待ってから送信してください。";
$l['pmdata_pm_flooding_one_second'] = "メッセージの送信間隔が短すぎます。前回の送信から 1 秒以上待ってから送信してください。";
?>