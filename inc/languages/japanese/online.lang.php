<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: online.lang.php 5788 2012-04-19 13:35:01Z Tomm $
 */

$l['nav_online'] = "オンライン";
$l['nav_onlinetoday'] = "今日のオンライン";
$l['viewing_announcements'] = "告知<a href=\"{1}\">{2}</a>を表示中";
$l['viewing_announcements2'] = "告知を読む";
$l['viewing_attachment'] = "添付ファイルを見る";
$l['viewing_attachment2'] = "スレッド<a href=\"{3}\">{2}</a><a href=\"attachment.php?aid={1}\" target=\"_blank\">添付ファイル</a>表示中";
$l['viewing_calendar'] = "<a href=\"calendar.php\">カレンダー</a>表示中";
$l['viewing_event'] = "イベント表示中";
$l['viewing_event2'] = "イベント<a href=\"{1}\">{2}</a>表示中";
$l['adding_event'] = "<a href=\"calendar.php?action=addevent\">イベント追加中</a>";
$l['editing_event'] = "イベント編集中";
$l['editing_post'] = "記事編集中";
$l['viewing_forum'] = "フォーラム閲覧中";
$l['viewing_forum2'] = "フォーラム<a href=\"{1}\">{2}</a>閲覧中";
$l['forum_redirect_to'] = "<a href=\"{1}\">{2}</a>へ転送中";
$l['viewing_index'] = "{1}<a href=\"index.php\">メインインデックス</a>";
$l['activating_account'] = "アカウントの有効化中";
$l['viewing_profile'] = "プロフィール閲覧中";
$l['viewing_profile2'] = "<a href=\"{1}\">{2}</a>のプロフィール閲覧中";
$l['registering'] = "<a href=\"member.php?action=register\">登録中</a>";
$l['logging_in'] = "<a href=\"member.php?action=login\">ログイン中</a>";
$l['logging_in_plain'] = "ログイン中";
$l['logging_out'] = "ログアウト中";
$l['emailing_user'] = "メール作成中";
$l['rating_user'] = "評価中";
$l['viewing_memberlist'] = "<a href=\"memberlist.php\">メンバーリスト</a>閲覧中";
$l['viewing_whoposted'] = "投稿者閲覧中";
$l['viewing_whoposted2'] = "スレッド<a href=\"{1}\">{2}</a>の投稿者閲覧中";
$l['marking_read'] = "<a href=\"misc.php?action=markread&amp;my_post_key={1}\">フォーラムを既読に設定中</a>";
$l['viewing_helpdocs'] = "<a href=\"misc.php?action=help\">ヘルプ</a>閲覧中";
$l['viewing_buddylist'] = "<a href=\"misc.php?action=buddypopup\">友人リスト</a>閲覧中";
$l['viewing_smilies'] = "<a href=\"misc.php?action=smilies\">スマイリーリスト</a>閲覧中";
$l['viewing_syndication'] = "<a href=\"misc.php?action=syndication\">RSS 配信</a>ページ閲覧中";
$l['replying_thread'] = "スレッドに返信中";
$l['replying_thread2'] = "スレッド<a href=\"{1}\">{2}</a>に返信中";
$l['posting_thread'] = "スレッドに投稿中";
$l['posting_thread2'] = "<a href=\"{1}\">{2}</a>に新規スレッド作成中";
$l['viewing_wol'] = "<a href=\"online.php\">オンラインユーザ</a>閲覧中";
$l['viewing_woltoday'] = "<a href=\"online.php?action=today\">今日のオンライン</a>閲覧中";
$l['creating_poll'] = "アンケート作成中";
$l['editing_poll'] = "アンケート編集中";
$l['viewing_pollresults'] = "アンケート結果閲覧中";
$l['voting_poll'] = "アンケートに回答中";
$l['using_modtools'] = "モデレータツール利用中";
$l['sending_pm'] = "プライベートメッセージ送信中";
$l['reading_pm'] = "プライベートメッセージ閲覧中";
$l['editing_pmfolders'] = "PMフォルダ編集中";
$l['using_pmsystem'] = "PMシステム利用中";
$l['reporting_post'] = "記事通報中";
$l['searching_forum'] = "{1}を<a href=\"search.php\">検索中</a>";
$l['reading_thread'] = "スレッド閲覧中";
$l['reading_thread2'] = "スレッド<a href=\"{1}\">{2}</a> {3}閲覧中";
$l['viewing_team'] = "フォーラムチーム閲覧中";
$l['viewing_stats'] = "フォーラム統計閲覧中";
$l['updating_profile'] = "<a href=\"usercp.php?action=profile\">プロフィール更新中</a>";
$l['updating_options'] = "<a href=\"usercp.php?action=options\">オプション更新中</a>";
$l['editing_signature'] = "<a href=\"usercp.php?action=editsig\">署名編集中</a>";
$l['changing_avatar'] = "<a href=\"usercp.php?action=avatar\">アバター変更中</a>";
$l['viewing_subscriptions'] = "<a href=\"usercp.php?action=subscriptions\">スレッドウォッチ</a>閲覧中";
$l['viewing_favorites'] = "<a href=\"usercp.php?action=favorites\">お気に入りスレッド</a>閲覧中";
$l['editing_pad'] = "<a href=\"usercp.php?action=notepad\">個人用メモ</a>編集中";
$l['editing_password'] = "<a href=\"usercp.php?action=password\">パスワード</a>変更中";
$l['user_cp'] = "<a href=\"usercp.php\">ユーザCP</a>閲覧中";
$l['viewing_portal'] = "<a href=\"portal.php\">ポータル</a>ページ閲覧中";
$l['viewing_noperms'] = "閲覧権限のないページを閲覧中";
$l['unknown_location'] = "<a href=\"{1}\">どこだかわかりません</a>";
$l['giving_reputation'] = "<a href=\"{1}\">{2}</a>を評価中";
$l['viewing_reputation_report'] = "<a href=\"{1}\">{2}の評価</a>閲覧中";
$l['viewing_reputation_report2'] = "評価閲覧中";
$l['member_resendactivation'] = "アカウント有効化メール再送中";
$l['member_lostpw'] = "<a href=\"member.php?action=lostpw\">紛失したパスワード</a>復旧中";
$l['sending_thread'] = "友人にスレッド送信中";
$l['guest'] = "ゲスト";
$l['page'] = "ページ";
$l['users_online'] = "オンラインユーザ";
$l['on_username'] = "ユーザ名";
$l['time'] = "時間";
$l['location'] = "場所";
$l['online_today'] = "今日のオンライン";
$l['refresh_page'] = "ページを再表示";
$l['online_online_plural'] = "ユーザ";
$l['online_online_singular'] = "ユーザ";
$l['online_member_plural'] = "メンバー";
$l['online_member_singular'] = "メンバー";
$l['online_anon_plural'] = "は";
$l['online_anon_singular'] = "は";
$l['online_guest_plural'] = "ゲスト";
$l['online_guest_singular'] = "ゲスト";
$l['online_count'] = "過去{3}分間に{1}{2}がアクティブ ({5}{4}人中、非表示が{6}人、{9}が{8}人)";
$l['ip'] = "IP:";
$l['resolves_to'] = "ホスト名:";
$l['if_resolvable'] = "(名前解決できれば)";
$l['admin_options'] = "管理者オプション:";
$l['search_regip_users'] = "このIPから登録したユーザを検索";
$l['search_postip_users'] = "このIPから投稿したユーザを検索";
$l['lookup'] = "[検索]";
$l['member_online_today'] = "今日のオンラインは<strong>1</strong>人";
$l['members_were_online_today'] = "今日のオンラインは<strong>{1}</strong>人";
$l['member_online_hidden'] = " ({1}人が非表示)";
$l['members_online_hidden'] = " ({1}人が非表示)";
$l['rating_thread'] = "スレッド評価中";
$l['viewing_imcenter'] = "IMセンター閲覧中";
$l['managing_favorites'] = "お気に入りスレッド管理中";
$l['managing_subscriptions'] = "スレッドウォッチ管理中";
$l['managing_group'] = "ユーザグループ管理中";
$l['viewing_modcp'] = "モデレータCP閲覧中";
$l['viewing_modlogs'] = "モデレータログ閲覧中";
$l['managing_announcements'] = "告知管理中";
$l['search_for_user'] = "ユーザ検索中";
$l['managing_warninglogs'] = "警告ログ管理中";
$l['searching_ips'] = "IP検索中";
$l['viewing_reports'] = "通報記事閲覧中";
$l['adding_announcement'] = "告知追加中";
$l['deleting_announcement'] = "告知削除中";
$l['editing_announcement'] = "告知編集中";
$l['managing_modqueue'] = "モデレートキュー管理中";
$l['editing_user_profiles'] = "ユーザプロフィール編集中";
$l['managing_bans'] = "追放管理中";
$l['revoking_warning'] = "警告解除中";
$l['warning_user'] = "警告発行中";
$l['viewing_warning'] = "警告閲覧中";
$l['managing_warnings'] = "警告管理中";
$l['changing_dst'] = "夏時間切り替え中";
$l['printing_thread'] = "スレッド印刷中";
$l['printing_thread2'] = "スレッド<a href=\"{1}\">{2}</a>印刷中";
$l['managing_buddyignorelist'] = "友人/無視リスト管理中";
?>
