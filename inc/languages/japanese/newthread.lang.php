<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: newthread.lang.php 5657 2011-11-27 12:32:47Z Tomm $
 */

$l['nav_newthread'] = "新規スレッド";
$l['newthread_in'] = "{1}の新規スレッド";
$l['post_new_thread'] = "新しくスレッドを作成";
$l['thread_subject'] = "スレッドの件名";
$l['your_message'] = "スレッドの本文:";
$l['post_options'] = "投稿オプション:";
$l['options_sig'] = "<b>署名:</b> 署名を挿入する。(登録ユーザのみ)";
$l['options_emailnotify'] = "<b>メール通知:</b> 新たに返信があったときメールで通知する (登録ユーザのみ)";
$l['options_disablesmilies'] = "<b>スマイリー無効:</b> この記事ではスマイリーを表示しない";
$l['post_thread'] = "送信";
$l['preview_post'] = "プレビュー";
$l['poll'] = "アンケート:";
$l['poll_desc'] = "オプションで、スレッドにアンケートを追加できます。";
$l['poll_check'] = "アンケートを作成する";
$l['num_options'] = "選択肢の数:";
$l['max_options'] = "(最大: {1})";
$l['mod_options'] = "モデレータオプション:";
$l['close_thread'] = "<b>スレッドを締め切る</b>: これ以上このスレッドに投稿できなくする";
$l['stick_thread'] = "<b>スレッドを固定:</b> スレッドをフォーラムの先頭に固定する";
$l['draft_saved'] = "新しいスレッドは下書き保存されました。<br />下書きの一覧に移動します。";
$l['image_verification'] = "画像認証";
$l['verification_note'] = "画像に表示されているテキストをテキストボックスに入力してください。自動投稿を防止するための処理です。";
$l['verification_subnote'] = "(大文字/小文字を区別)";
$l['invalid_captcha'] = "像認証コードが間違っています。画像に表示されているとおりに入力してください。";
$l['error_post_already_submitted'] = "すでにスレッドに返信として投稿済みです。スレッドに移動して投稿した返信をご覧ください。";
$l['no_prefix'] = "プリフィックスなし";
$l['forum_rules'] = "{1} - ルール";
$l['multiquote_external_one'] = "別のスレッドの記事が1個選択されています。";
$l['multiquote_external'] = "別のスレッドの記事が{1}個選択されています。";
$l['multiquote_external_one_deselect'] = "選択を解除";
$l['multiquote_external_deselect'] = "選択を解除";
$l['multiquote_external_one_quote'] = "この記事も引用";
$l['multiquote_external_quote'] = "この記事も引用";
$l['redirect_newthread'] = "スレッドは投稿されました。";
$l['redirect_newthread_poll'] = "<br />アンケート設定のページに移動します。";
$l['redirect_newthread_moderation'] = "<br />管理者の設定により、すべての記事はモデレートが必要です。スレッドに戻ります。";
$l['redirect_newthread_thread'] = "<br />新しいスレッドに移動します。";
$l['invalidthread'] = "指定された下書きが存在しません。または開く権限がありません。";
?>
