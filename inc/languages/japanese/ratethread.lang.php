<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: ratethread.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['redirect_threadrated'] = "スレッドの評価を保存しました。スレッドに戻ります。";
$l['error_invalidrating'] = "選択されたスレッドの評価は無効です。戻ってもう一度お試しください。";
$l['error_alreadyratedthread'] = "このスレッドはすでに評価済みです。";
$l['rating_votes_average'] = "評価数{1} - 平均{2} (満点5)";
$l['one_star'] = "1 (満点5)";
$l['two_stars'] = "2 (満点5)";
$l['three_stars'] = "3 (満点5)";
$l['four_stars'] = "4 (満点5)";
$l['five_stars'] = "5 (満点5)";
$l['rating_added'] = "評価が追加されました。";
?>
