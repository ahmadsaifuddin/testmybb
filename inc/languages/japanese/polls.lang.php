<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: polls.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['nav_postpoll'] = "アンケートを作成";
$l['nav_editpoll'] = "アンケートを編集";
$l['nav_pollresults'] = "アンケート結果";
$l['edit_poll'] = "アンケートを編集";
$l['delete_poll'] = "アンケートを削除";
$l['delete_q'] = "削除しますか？";
$l['delete_note'] = "アンケートを削除するには、左のチェックボックスをチェックして右のボタンをクリックしてください。";
$l['delete_note2'] = "<b>注意:</b> 一度削除したアンケートをもとに戻すことはできません。";
$l['question'] = "質問:";
$l['num_options'] = "選択肢の数:";
$l['max_options'] = "最大:";
$l['poll_options'] = "アンケートの選択肢:";
$l['update_options'] = "選択肢の数を更新";
$l['poll_options_note'] = "選択肢は簡潔にわかりやすくしてください。";
$l['options'] = "オプション:";
$l['option_multiple'] = "<b>複数回答の許可:</b> 複数の回答を許可する";
$l['option_public'] = "<b>公開アンケート:</b> 誰がどの選択肢を選択したか公開する";
$l['option_closed'] = "<b>アンケートを締め切る:</b> これ以上回答を受け付けない";
$l['poll_timeout'] = "アンケート期間:";
$l['timeout_note'] = "アンケートを締め切るまでの日数。<br />(0を指定すると締め切りを設定しません。)";
$l['days_after'] = "日後:";
$l['update_poll'] = "アンケートを更新";
$l['option'] = "回答";
$l['votes'] = "回答数:";
$l['post_new_poll'] = "新しくアンケートを作成";
$l['days'] = "日間";
$l['poll_results'] = "アンケート結果";
$l['poll_total'] = "合計:";
$l['poll_votes'] = "アンケート";
$l['redirect_pollposted'] = "アンケートが作成されました。<br />スレッドに戻ります。";
$l['redirect_pollpostedmoderated'] = "アンケートは作成されましたが、スレッドがまだモデレートされていません。<br />フォーラムに戻ります。";
$l['redirect_pollupdated'] = "アンケートが更新されました。<br />スレッドに戻ります。";
$l['redirect_votethanks'] = "アンケートありがとうございました。<br />スレッドに戻ります。";
$l['redirect_unvoted'] = "アンケートは削除されました。<br />スレッドに戻ります。";
$l['redirect_polldeleted'] = "スレッドからアンケートが削除されました。<br />スレッドに戻ります。";
$l['error_polloptiontoolong'] = "入力された選択肢の中に長すぎるものがあります。戻って短くしてください。";
$l['error_noquestionoptions'] = "質問が入力されていません。または選択肢の数が不足しています。選択肢は最低でも2以上必要です。<br />戻って修正してください。";
$l['error_pollalready'] = "すでにアンケートがあります。";
$l['error_nopolloptions'] = "指定されたアンケートが間違っています。または存在しません。";
$l['error_alreadyvoted'] = "すでに回答済みです。";
$l['error_notvoted'] = "まだ回答していません。";
$l['error_invalidpoll'] = "指定されたアンケートが間違っています。または存在しません。";
$l['error_pollclosed'] = "アンケートが締め切られているため、回答できません。";
$l['poll_deleted'] = "削除されたアンケート";
$l['poll_edited'] = "編集されたアンケート";
?>
