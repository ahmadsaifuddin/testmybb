<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: stats.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['nav_stats'] = "掲示板の統計";
$l['board_stats'] = "掲示板の統計";
$l['none'] = "なし";
$l['nobody'] = "なし";
$l['totals'] = "合計";
$l['averages'] = "平均";
$l['posts'] = "投稿数:";
$l['threads'] = "スレッド数:";
$l['members'] = "メンバー数:";
$l['ppd'] = "1日当たりの投稿数:";
$l['tpd'] = "1日当たりのスレッド数:";
$l['mpd'] = "1日当たりのメンバ登録数:";
$l['ppm'] = "ひとり当たりの投稿数:";
$l['rpt'] = "スレッド当たりの記事数:";
$l['no_posts'] = "なし";
$l['general'] = "全般";
$l['newest_member'] = "最新のメンバー:";
$l['members_posted'] = "投稿したメンバー:";
$l['todays_top_poster'] = "本日のトップ投稿者: <b>{1}</b> (<b>{2}</b> posts)";
$l['popular_forum'] = "トップ人気フォーラム: <b>{1}</b> (記事数<b>{2}</b> 、スレッド数<b>{3}</b>)";
$l['most_popular'] = "人気ランキング";
$l['most_replied_threads'] = "最も返信の多いスレッド";
$l['most_viewed_threads'] = "最もよく読まれたスレッド";
$l['not_enough_info_stats'] = "Sorry, but there is not enough information on this board to generate statistics. Before statistics can be generated this board needs to contain at least 1 member and 1 thread.";
$l['replies'] = "記事";
$l['views'] = "回";
?>