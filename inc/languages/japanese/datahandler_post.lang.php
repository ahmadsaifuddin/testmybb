<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 *
 * $Id$
 */

$l['postdata_invalid_user_id'] = "ユーザIDが存在しません。正しいユーザIDを指定してください。";
$l['postdata_firstpost_no_subject'] = "スレッドに件名がつけられていません。件名を入力してください。";
$l['postdata_missing_subject'] = "件名がありません。件名を入力してください。";
$l['postdata_missing_message'] = "本文がありません。本文を入力してください。";
$l['postdata_message_too_long'] = "本文が長すぎます。{1} 文字以内におさめてください。＋（現在 {2} 文字）";
$l['postdata_message_too_short'] = "本文が短すぎます。最低でも {1} 文字以上にしてください。";
$l['postdata_subject_too_long'] = "件名が長すぎます。85文字以内におさめてください。 （現在 {1} 文字）";
$l['postdata_post_flooding'] = "投稿の間隔が短すぎます。前回の投稿から {1} 秒以上の間隔をあけて投稿してください。";
$l['postdata_post_flooding_one_second'] = "投稿の間隔が短すぎます。前回の投稿から 1 秒以上の間隔をあけて投稿してください。";
$l['postdata_too_many_images'] = "画像の数が多すぎます。投稿あたりの画像は {2} までに制限されていますが {1} の画像が添付されています。制限内に収まるよう、数を減らしてください。";
$l['postdata_too_many_videos'] = "動画の数が多すぎます。投稿あたりの動画は {2} までに制限されていますが {1} の動画が添付されています。制限内に収まるよう、数を減らしてください。";
$l['postdata_invalid_prefix'] = "選択したプリフィックスが間違っています。正しいプリフィックスを選択してください。";
$l['thread_closed'] = "スレッドは締め切られています";
$l['thread_opened'] = "スレッドは返信受付中です";
$l['thread_stuck'] = "スレッドは固定されています";
$l['thread_unstuck'] = "スレッドは固定されていません";
?>
