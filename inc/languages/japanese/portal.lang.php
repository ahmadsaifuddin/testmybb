<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: portal.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['nav_portal'] = "ポータル";
$l['posted_by'] = "投稿者:";
$l['replies'] = "返信数";
$l['no_replies'] = "返信はありません";
$l['latest_threads'] = "最新のスレッド";
$l['latest_threads_replies'] = "返信数:";
$l['latest_threads_views'] = "閲覧数:";
$l['latest_threads_lastpost'] = "最後の投稿者:";
$l['private_messages'] = "プライベートメッセージ";
$l['pms_received_new'] = "{1}の未読メッセージは<b>{2}</b>通です。";
$l['pms_unread'] = "未読メッセージ";
$l['pms_total'] = "全メッセージ";
$l['search_forums'] = "フォーラムを検索";
$l['advanced_search'] = "高度な検索";
$l['forum_stats'] = "フォーラム統計";
$l['num_members'] = "メンバー数:";
$l['latest_member'] = "最新のメンバー:";
$l['num_threads'] = "フォーラムのスレッド数:";
$l['num_posts'] = "フォーラムの記事数:";
$l['full_stats'] = "詳しい統計";
$l['welcome'] = "{1}";
$l['guest'] = "ゲスト";
$l['guest_welcome_registration'] = "投稿の前に<a href=\"{1}\">登録</a>してください。";
$l['username'] = "ユーザ名";
$l['password'] = "パスワード";
$l['login'] = "ログイン";
$l['member_welcome_lastvisit'] = "最後のログイン:";
$l['since_then'] = "前回のログアウト後の投稿は";
$l['new_announcements'] = "新しい告知: {1}";
$l['new_announcement'] = "新しい告知: 1";
$l['new_threads'] = "新規スレッド: {1}";
$l['new_thread'] = "新規スレッド: 1";
$l['new_posts'] = "新しく投稿された記事: {1}";
$l['new_post'] = "新しく投稿された記事: 1";
$l['view_new'] = "新しい記事を読む";
$l['view_todays'] = "今日投稿された記事を読む";
$l['online'] = "オンラインユーザ";
$l['online_user'] = "現在のオンラインユーザは1人です。";
$l['online_users'] = "現在のオンラインユーザは<b>{1}</b>人です。";
$l['online_counts'] = "メンバー<b>{1}</b>人 | ゲスト<b>{2}</b>人";
$l['no_one'] = "誰もいません";
$l['print_this_item'] = "この項目を印刷";
$l['send_to_friend'] = "この項目を友人に送信する";
?>
