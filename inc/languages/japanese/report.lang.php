<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: report.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['report_post'] = "記事を通報";
$l['report_to_mod'] = "この記事をモデレータに通報";
$l['only_report'] = "スパム、広告、荒らしであると判断した場合にのみ通報してください。";
$l['report_reason'] = "通報の理由:";
$l['thank_you'] = "ありがとうございました。";
$l['post_reported'] = "記事が通報されました。ウィンドウを閉じてください。";
$l['report_error'] = "エラー";
$l['no_reason'] = "理由なしに通報することはできません。";
$l['go_back'] = "戻る";
$l['close_window'] = "閉じる";
?>