<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 * 
 * $Id: announcements.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */

$l['nav_announcements'] = "フォーラム告知";
$l['announcements'] = "告知";
$l['forum_announcement'] = "フォーラム告知: {1}";
$l['error_invalidannouncement'] = "指定された告知が正しくありません。";
$l['announcement_edit'] = "告知を編集";
$l['announcement_qdelete'] = "告知を削除";
$l['announcement_quickdelete_confirm'] = "本当にこの告知を削除しますか？";
?>