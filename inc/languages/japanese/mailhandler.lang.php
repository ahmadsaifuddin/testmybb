<?php
/**
 * MyBB 1.6 English Language Pack
 * Copyright 2010 MyBB Group, All Rights Reserved
 *
 * $Id: mailhandler.lang.php 5297 2010-12-28 22:01:14Z Tomm $
 */
 
$l['error_no_connection'] = "サーバへ接続中に障害が発生しました。: ";
$l['error_no_message'] = "本文が指定されていません。";
$l['error_no_subject'] = "件名が指定されていません。";
$l['error_no_recipient'] = "宛先が指定されていません。";
$l['error_not_sent'] = "PHPの機能によるメール送信中に障害が発生しました。";
$l['error_status_missmatch'] = "サーバから予期しないステータスが返されました。ステータス: ";
$l['error_data_not_sent'] = "データをサーバに送信できませんでした: ";
$l['error_occurred'] = "障害が発生しました。次のエラーを修正してから処理を継続してください。<br />";
?>