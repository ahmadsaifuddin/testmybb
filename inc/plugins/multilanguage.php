<?php

function multilanguage_info() {
    global $lang;
    
    $lang->load("multilanguage");
    
    return array(
		'name'			=> $lang->multilanguage,
		'description'	=> $lang->multilanguage_desc,
		'website'		=> 'http://www.chack1172.altervista.org/projects.php?action=view&id=10',
		'author'		=> 'chack1172',
		'authorsite'	=> 'http://www.chack1172.altervista.org',
		'version'		=> '1.0',
		'compatibility'	=> '18*',
		'codename'		=> 'multilanguage'
	);
}

if(defined("IN_ADMINCP")) {
    $plugins->add_hook("admin_forum_menu", "multilanguage_menu");
    $plugins->add_hook("admin_forum_action_handler", "multilanguage_module");
}
else
    $plugins->add_hook("build_forumbits_forum", "multilanguage_change");

function multilanguage_activate() {}
function multilanguage_deactivate() {}

function multilanguage_change(&$forum) {
    global $lang;
    if(file_exists($lang->path."/".$lang->language."/multilanguage.lang.php")) {
        $lang->load("multilanguage");
        $name = "forum_".$forum['fid'];
        $desc = $name."_desc";
        
        if(!empty($lang->$name))
            $forum['name'] = $lang->$name;
        
        if(!empty($lang->$desc))
            $forum['description'] = $lang->$desc;
    }
}

function multilanguage_menu(&$sub_menu) {
    global $lang;
    $lang->load("multilanguage");
    $sub_menu[] = array("id" => "multilanguage", "title" => $lang->multilanguage, "link" => "index.php?module=forum-multilanguage");
}

function multilanguage_module(&$actions) {
    $actions['multilanguage'] = array('active' => 'multilanguage', 'file' => 'multilanguage.php');
}