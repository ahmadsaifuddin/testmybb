<?php
if(!defined("IN_MYBB"))
	die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");

$lang->load("multilanguage");
$page->add_breadcrumb_item($lang->multilanguage, "index.php?module=forum-multilanguage");

if($mybb->input['action'] == "edit") {
    $editlang = basename($mybb->input['lang']);
    
    $page->add_breadcrumb_item($editlang, "index.php?module=forum-multilanguage&action=edit&lang={$editlang}");
    
    if(!$lang->language_exists($editlang))
        header("Location: index.php?module=forum-multilanguage");
    
    $file = $lang->path."/".$editlang."/multilanguage.lang.php";
    
    if($mybb->request_method == "post") {
        $content = "<?php\n";
        foreach($mybb->input['translation'] as $key => $value)
            if(!empty($value))
                $content .= "\$l['{$key}'] = '".str_replace("'", "\'", $value)."';\n";
        
        if($f = @fopen($file, "w")) {
            flock($f, LOCK_EX);
            fwrite($f, $content);
            flock($f, LOCK_UN);
            fclose($f);

            flash_message($lang->multilanguage_saved, "success");
        }
        else
            flash_message($lang->multilanguage_error, "error");
    }
    
    $page->output_header("{$editlang} - {$lang->multilanguage}");
    
    $form = new Form("index.php?module=forum-multilanguage&action=edit&lang={$editlang}", "post", "translations");
    $lang->multilanguage_translation = $lang->sprintf($lang->multilanguage_translation, $editlang);
    $form_container = new FormContainer($lang->multilanguage_translation);

    $form_container->output_row_header("{$lang->multilanguage_default}", array("style" => "width: 50%"));
    $form_container->output_row_header("{$lang->multilanguage_title}", array("style" => "width: 25%", "class" => "align_center"));
    $form_container->output_row_header("{$lang->multilanguage_description}", array("style" => "width: 25%", "class" => "align_center"));
    
    if(file_exists($file))
        include($file);
    
    build_forums_list($form_container);

    $form_container->end();

    $buttons[] = $form->generate_submit_button($lang->multilanguage_save, array("name" => "save_images"));
    $form->output_submit_wrapper($buttons);

    $form->end();
    
    $page->output_footer();
}

if(!$mybb->input['action']) {
    $page->output_header($lang->multilanguage);
    
    $table = new Table;
    $table->construct_header($lang->languagevar);
    $table->construct_header($lang->controls, array("class" => "align_center", "width" => 155));

    $languages = $lang->get_languages();
    asort($languages);

    foreach($languages as $key => $langname)
    {
        include MYBB_ROOT."inc/languages/".$key.".php";
        $table->construct_cell("<strong>".preg_replace("<\?|\?>", "<span>?</span>", $langinfo['name'])."</strong>");
        $table->construct_cell("<a href='index.php?module=forum-multilanguage&amp;action=edit&amp;lang={$key}'>{$lang->multilanguage_translate}</a>", array("class" => "align_center"));
        $table->construct_row();
    }

    $table->output($lang->multilanguage_packets);

    $page->output_footer();
}

function build_forums_list(&$form_container, $pid=0, $depth=1)
{
	global $mybb, $lang, $l;
	static $forums_by_parent;

	if(!is_array($forums_by_parent))
	{
		$forum_cache = cache_forums();

		foreach($forum_cache as $forum)
			$forums_by_parent[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
	}

	if(!is_array($forums_by_parent[$pid]))
		return;

	foreach($forums_by_parent[$pid] as $children)
	{
		foreach($children as $forum)
		{
			$forum['name'] = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $forum['name']);

			if($forum['type'] == "c")
			{

				$form_container->output_cell("<div style=\"padding-left: ".(40*($depth-1))."px;\"><strong>{$forum['name']}</strong></div>");
                $forum_name = "forum_{$forum['fid']}";
				$form_container->output_cell("<input type=\"text\" name=\"translation[forum_".$forum['fid']."]\" value=\"".$l[$forum_name]."\" class=\"text_input\" />", array("class" => "align_center"));
                $form_container->output_cell("");
                
				$form_container->construct_row();

				if($forums_by_parent[$forum['fid']])
					build_forums_list($form_container, $forum['fid'], $depth+1);
			}
			elseif($forum['type'] == "f")
			{
				if($forum['description'])
				{
					$forum['description'] = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $forum['description']);
           			$forum['description'] = "<br /><small>".$forum['description']."</small>";
       			}

				$form_container->output_cell("<div style=\"padding-left: ".(40*($depth-1))."px;\">{$forum['name']}{$forum['description']}</div>");
                $forum_name = "forum_{$forum['fid']}";
                $forum_desc = "forum_{$forum['fid']}_desc";
				$form_container->output_cell("<input type=\"text\" name=\"translation[forum_".$forum['fid']."]\" value=\"".$l[$forum_name]."\" class=\"text_input\" />", array("class" => "align_center"));
				$form_container->output_cell("<textarea name=\"translation[forum_".$forum['fid']."_desc]\" class=\"text_input\">".$l[$forum_desc]."</textarea>", array("class" => "align_center"));


				$form_container->construct_row();

				if(isset($forums_by_parent[$forum['fid']]))
					build_forums_list($form_container, $forum['fid'], $depth+1);
			}
		}
	}
}
